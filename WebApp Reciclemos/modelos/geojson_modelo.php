<?php

class geoJSON
{
    private $db;
    public $datosGeoJSON;

    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->datosGeoJSON = array();
    }

    public function crearTodos()
    {
        $consulta =$this->db->query("SELECT * FROM punto_verde, gestores, categorias 
                                     WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR 
                                     AND punto_verde.TIPO_PTO1=categorias.TIPO_PUNTO");
        $geojson = array('type' => 'FeatureCollection', 'features' => array());
        while ($row = $consulta->fetch(PDO::FETCH_ASSOC)) {
            $marker = array(
                'type' => 'Feature',
                   "geometry" => array(
                        'type' => 'Point',
                        'coordinates' => array(
                             floatval($row['LONGITUD']),
                             floatval($row['LATITUD'])
                        )
                    ),
                       "properties" => array(
                               'Nombre'               => $row['NOMBRE_PTO'],
                               'Ubicacion'            => $row['UBICACION_PTO'],
                               'TipodePunto'          => $row['TIPO_PUNTO'],
                               'Elementosquerecibe'   => $row['ELEMENTOS_RECIBE'],
                               'Horario'              => $row['HORARIO_PTO'],
                               'Contacto'             => $row['NOMBRE_GESTOR'],
                               'Emaildecontacto'      => $row['CONTACTO_GESTOR'],
                               'Estado'			      => $row['ESTADO'],
                               'Id_Punto'             => $row['ID_PUNTO'])
              
            );
            array_push($geojson['features'], $marker);
        }
        fwrite(fopen("datosGeoJSON.js", 'w'), 'var datosGeoJSON='.json_encode($geojson, JSON_UNESCAPED_UNICODE));
    }

    public function crearUnico($ID_PUNTO)
    {
        $consulta =$this->db->query("SELECT * FROM punto_verde, gestores, categorias WHERE punto_verde.ID_PUNTO = '$ID_PUNTO' AND punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND punto_verde.TIPO_PTO1=categorias.TIPO_PUNTO");
        $geojson = array('type' => 'FeatureCollection', 'features' => array());
        while ($row = $consulta->fetch(PDO::FETCH_ASSOC)) {
            $marker = array(
                'type' => 'Feature',
                   "geometry" => array(
                        'type' => 'Point',
                        'coordinates' => array(
                             floatval($row['LONGITUD']),
                             floatval($row['LATITUD'])
                        )
                    ),
                       "properties" => array(
                               'Nombre'               => $row['NOMBRE_PTO'],
                               'Ubicacion'            => $row['UBICACION_PTO'],
                               'TipodePunto'          => $row['TIPO_PUNTO'],
                               'Elementosquerecibe'   => $row['ELEMENTOS_RECIBE'],
                               'Horario'              => $row['HORARIO_PTO'],
                               'Contacto'             => $row['NOMBRE_GESTOR'],
                               'Emaildecontacto'      => $row['CONTACTO_GESTOR'],
                               'Estado'			   => $row['ESTADO'],
                               'Id_Punto'             => $row['ID_PUNTO'])
              
            );
            array_push($geojson['features'], $marker);
        }
        fwrite(fopen("puntoGeoJSON.js", 'w'), 'var puntoGeoJSON='.json_encode($geojson, JSON_UNESCAPED_UNICODE));
    }
}
