<?php

class Gestor
{
    private $db;
    public $gestor;

    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->gestor = array();
    }

    public function getGestor()
    {
        $consulta =$this->db->query("SELECT * FROM gestores");
        $this->gestor =$consulta;
        return $this->gestor;
    }

    public function getCantidadGestor()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM gestores");
        $this->gestor = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->gestor;
    }

    public function getGestorPorId($ID_GESTOR)
    {
        $consulta =$this->db->query("SELECT * FROM gestores WHERE ID_GESTOR = '$ID_GESTOR'");
        $this->gestor =$consulta;
        return $this->gestor;
    }

    public function addGestor($NOMBRE_GESTOR, $CONTACTO_GESTOR, $CONTRASENA_GESTOR)
    {
        $consulta =$this->db->query("INSERT INTO gestores VALUES (null, '$NOMBRE_GESTOR', '$CONTACTO_GESTOR', '$CONTRASENA_GESTOR')");
    }

    public function updateGestor($ID_GESTOR, $NOMBRE_GESTOR, $CONTACTO_GESTOR, $CONTRASENA_GESTOR)
    {
        $consulta =$this->db->query("UPDATE gestores SET NOMBRE_GESTOR='$NOMBRE_GESTOR', CONTACTO_GESTOR='$CONTACTO_GESTOR', PASSWORD_GESTOR='$CONTRASENA_GESTOR' WHERE ID_GESTOR='$ID_GESTOR'");
    }

    public function deleteGestor($ID_GESTOR)
    {
        $consulta =$this->db->query("DELETE FROM gestores WHERE ID_GESTOR='$ID_GESTOR'");
    }
}
