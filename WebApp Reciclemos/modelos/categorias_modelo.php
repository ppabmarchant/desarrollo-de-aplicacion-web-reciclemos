<?php

class Categoria
{
    private $db;
    public $categoria;

    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->puntos = array();
    }

    public function getCategoria()
    {
        $consulta = $this->db->query("SELECT * FROM categorias");
        $this->categoria =$consulta;
        return $this->categoria;
    }

    public function getCantidadCategoria()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM categorias");
        $this->categoria = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->categoria;
    }

    public function getCategoriaPorId($TIPO_PUNTO)
    {
        $consulta = $this->db->query("SELECT * FROM categorias WHERE TIPO_PUNTO = '$TIPO_PUNTO'");
        $this->categoria =$consulta;
        return $this->categoria;
    }

    public function addCategoria($TIPO_PUNTO, $ELEMENTOS_RECIBE)
    {
        $consulta =$this->db->query("INSERT INTO categorias VALUES ('$TIPO_PUNTO', '$ELEMENTOS_RECIBE')");
    }

    public function updateCategoria($TIPO_PUNTO, $TIPO_PUNTO_INICIAL, $ELEMENTOS_RECIBE)
    {
        $consulta =$this->db->query("UPDATE categorias SET TIPO_PUNTO='$TIPO_PUNTO', ELEMENTOS_RECIBE='$ELEMENTOS_RECIBE' WHERE TIPO_PUNTO='$TIPO_PUNTO_INICIAL'");
    }

    public function deleteCategoría($TIPO_PUNTO)
    {
        $consulta =$this->db->query("DELETE FROM categorias WHERE TIPO_PUNTO='$TIPO_PUNTO'");
    }
}
