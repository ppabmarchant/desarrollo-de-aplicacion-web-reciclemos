<?php

class Login
{
    private $db;

    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
    }

    public function loginSEREMI($usuario, $contrasena)
    {
        $consulta = $this->db->query("SELECT COUNT(*) coincidencia FROM administrador WHERE USER_ADMIN = '$usuario' AND PASSWORD_ADMIN = '$contrasena'");
        $respuesta = $consulta->fetch(PDO::FETCH_ASSOC);
        if ($respuesta['coincidencia'] > 0) {
            session_start();
            $_SESSION['username_admin'] = $usuario;
            header("location: ../vistas/index_admin.php");
        } else {
            $_SESSION['error'] = "Credenciales no válidas, intente nuevamente";
            header("location: ../vistas/Login_vista.php");
        }
    }

    public function loginGestor($usuario, $contrasena)
    {
        $consulta = $this->db->query("SELECT COUNT(*) coincidencia FROM gestores WHERE NOMBRE_GESTOR = '$usuario' AND PASSWORD_GESTOR = '$contrasena'");
        $respuesta = $consulta->fetch(PDO::FETCH_ASSOC);
        if ($respuesta['coincidencia'] > 0) {
            $consulta2 = $this->db->query("SELECT ID_GESTOR FROM gestores WHERE NOMBRE_GESTOR = '$usuario'");
            session_start();
            $resultado = $consulta2->fetch(PDO::FETCH_ASSOC);
            $id_gestor = $resultado['ID_GESTOR'];
            $_SESSION['id_gestor'] = $id_gestor;
            $_SESSION['username_gestor'] = $usuario;
            header("location: ../vistas/index_gestor.php");
        } else {
            $_SESSION['error'] = "Credenciales no válidas, intente nuevamente";
            header("location: ../vistas/Login_vista.php");
        }
    }

    public function cerrarSesion()
    {
        session_start();
        session_destroy();
        header("location: ../index.php");
    }
}
