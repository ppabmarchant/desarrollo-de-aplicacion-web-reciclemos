<?php

class Solicitud
{
    private $db;
    public $solicitud;
    
    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->solicitud = array();
    }

    public function add($NOMBRE_PTO, $UBICACION_PTO, $LATITUD, $LONGITUD, $TIPO_PTO, $ID_GESTOR, $HORARIO_PTO)
    {
        $consulta =$this->db->query("INSERT INTO solicitudes 
					VALUES (null,
                    '$NOMBRE_PTO',
					'$UBICACION_PTO',
					'$LATITUD',
					'$LONGITUD',
					'$TIPO_PTO',
					'$ID_GESTOR',
					'$HORARIO_PTO',
					'PENDIENTE')");
    }

    public function get()
    {
        $consulta = $this->db->query("SELECT solicitudes.ID_SOLICITUD, 
            solicitudes.NOMBRE_PTO, 
            solicitudes.UBICACION_PTO, 
            solicitudes.LATITUD, 
            solicitudes.LONGITUD, 
            solicitudes.HORARIO_PTO,
            solicitudes.ESTADO, 
            solicitudes.TIPO_PTO1,
            categorias.ELEMENTOS_RECIBE,
            gestores.NOMBRE_GESTOR, 
            gestores.CONTACTO_GESTOR 
            FROM solicitudes, gestores, categorias
            WHERE solicitudes.ID_GESTOR1=gestores.ID_GESTOR
            AND solicitudes.TIPO_PTO1=categorias.TIPO_PUNTO
            ORDER BY solicitudes.ID_SOLICITUD");
        $this->solicitud =$consulta;
        return $this->solicitud;
    }

    public function getByGestor($ID_GESTOR)
    {
        $consulta = $this->db->query("SELECT * FROM solicitudes WHERE ID_GESTOR1 = '$ID_GESTOR' ORDER BY ID_SOLICITUD");
        $this->solicitud =$consulta;
        return $this->solicitud;
    }

    public function getById($ID_SOLICITUD)
    {
        $consulta = $this->db->query("SELECT * FROM solicitudes, gestores, categorias
					WHERE solicitudes.ID_GESTOR1 = gestores.ID_GESTOR 
					AND categorias.TIPO_PUNTO = solicitudes.TIPO_PTO1 
					AND solicitudes.ID_SOLICITUD = '$ID_SOLICITUD' 
					ORDER BY solicitudes.ID_SOLICITUD");
        $this->solicitud =$consulta;
        return $this->solicitud;
    }

    public function count()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM solicitudes");
        $this->solicitud = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->solicitud;
    }

    public function countByGestor($ID_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM solicitudes WHERE ID_GESTOR = '$ID_GESTOR'");
        $this->solicitud = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->solicitud;
    }

    public function countPendientes()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM solicitudes WHERE ESTADO = 'PENDIENTE'");
        $this->solicitud = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->solicitud;
    }
    public function countPendientesByGestor($ID_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM solicitudes WHERE ESTADO = 'PENDIENTE' AND ID_GESTOR1='$ID_GESTOR'");
        $this->solicitud = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->solicitud;
    }
    public function approve($ID_SOLICITUD)
    {
        $this->db->query("UPDATE solicitudes SET ESTADO = 'APROBADO' WHERE ID_SOLICITUD='$ID_SOLICITUD'");
    }

    public function delete($ID_SOLICITUD)
    {
        $this->db->query("DELETE FROM solicitudes WHERE ID_SOLICITUD='$ID_SOLICITUD'");
    }

    public function refuse($ID_SOLICITUD)
    {
        $this->db->query("UPDATE solicitudes SET ESTADO = 'RECHAZADO' WHERE ID_SOLICITUD='$ID_SOLICITUD'");
    }
}
