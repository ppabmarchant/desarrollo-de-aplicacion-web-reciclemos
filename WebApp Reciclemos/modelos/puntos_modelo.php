<?php

class Puntos
{
    private $db;
    public $puntos;

    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->puntos = array();
    }

    public function getPuntos()
    {
        $consulta =$this->db->query("SELECT punto_verde.ID_PUNTO, 
            punto_verde.NOMBRE_PTO, 
            punto_verde.UBICACION_PTO, 
            punto_verde.LATITUD, 
            punto_verde.LONGITUD, 
            punto_verde.HORARIO_PTO,
            punto_verde.ESTADO, 
            categorias.TIPO_PUNTO, 
            categorias.ELEMENTOS_RECIBE, 
            gestores.NOMBRE_GESTOR, 
            gestores.CONTACTO_GESTOR 
            FROM punto_verde, gestores, categorias 
            WHERE punto_verde.TIPO_PTO1=categorias.TIPO_PUNTO 
            AND punto_verde.ID_GESTOR1=gestores.ID_GESTOR 
            ORDER BY punto_verde.ID_PUNTO");
        $this->puntos =$consulta;
        return $this->puntos;
    }

    public function getCantidadPuntos()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntosGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntosHabilitados()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde WHERE ESTADO = 'Habilitado'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntosHabilitadosGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR' AND punto_verde.ESTADO = 'Habilitado'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntosInhabilitados()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde WHERE ESTADO = 'Inhabilitado'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntosInhabilitadosGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR' AND punto_verde.ESTADO = 'Inhabilitado'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntoLimpio()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde WHERE TIPO_PTO1 = 'PuntoLimpio'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadPuntoLimpioGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR' AND punto_verde.TIPO_PTO1 = 'Punto Limpio'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadCampana()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde WHERE TIPO_PTO1 = 'Campana'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadCampanaGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR' AND punto_verde.TIPO_PTO1 = 'Campana'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadContenedor()
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad 
            FROM punto_verde 
            WHERE TIPO_PTO1 = 'Contenedor'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getCantidadContenedorGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad 
            FROM punto_verde, gestores 
            WHERE punto_verde.ID_GESTOR1 = gestores.ID_GESTOR 
            AND gestores.NOMBRE_GESTOR='$USERNAME_GESTOR' 
            AND punto_verde.TIPO_PTO1 = 'Contenedor'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getPuntosPorId($id_punto)
    {
        $consulta = $this->db->query("SELECT 
            punto_verde.ID_PUNTO, 
            punto_verde.NOMBRE_PTO, 
            punto_verde.UBICACION_PTO, 
            punto_verde.LATITUD, 
            punto_verde.LONGITUD, 
            punto_verde.HORARIO_PTO,
            punto_verde.ESTADO, 
            categorias.TIPO_PUNTO, 
            categorias.ELEMENTOS_RECIBE, 
            gestores.ID_GESTOR, 
            gestores.NOMBRE_GESTOR, 
            gestores.CONTACTO_GESTOR 
            FROM punto_verde, gestores, categorias 
            WHERE punto_verde.ID_PUNTO='$id_punto' 
            AND punto_verde.TIPO_PTO1=categorias.TIPO_PUNTO 
            AND punto_verde.ID_GESTOR1=gestores.ID_GESTOR
            ORDER BY punto_verde.ID_PUNTO");
        $this->puntos = $consulta;
        return $this->puntos;
    }

    public function getPuntosPorGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT * FROM punto_verde, gestores, categorias
            WHERE punto_verde.TIPO_PTO1 = categorias.TIPO_PUNTO 
            AND punto_verde.ID_GESTOR1 = gestores.ID_GESTOR 
            AND gestores.NOMBRE_GESTOR = '$USERNAME_GESTOR' 
            ORDER BY punto_verde.ID_PUNTO");
        $this->puntos = $consulta;
        return $this->puntos;
    }

    public function addPuntos($nombre, $ubicacion, $latitud, $longitud, $tipoPunto, $idGestor, $horario)
    {
        require_once "geojson_modelo.php";
        $consulta =$this->db->query("INSERT INTO punto_verde 
            VALUES (null, '$nombre', 
                    '$ubicacion', 
                    '$latitud',
                    '$longitud', 
                    '$tipoPunto',
                    '$idGestor',
                    '$horario',
                    'Habilitado')");
        $obj = new geoJSON();
        $actualizar = $obj->crearTodos();
    }

    public function updatePuntos($id, $nombre, $ubicacion, $latitud, $longitud, $tipoPunto, $idGestor, $horario)
    {
        require_once "geojson_modelo.php";
        $consulta =$this->db->query("UPDATE punto_verde SET 
                    NOMBRE_PTO    = '$nombre', 
                    UBICACION_PTO = '$ubicacion', 
                    LATITUD       = '$latitud',
                    LONGITUD      = '$longitud', 
                    TIPO_PTO1     = '$tipoPunto',
                    ID_GESTOR1     = '$idGestor',
                    HORARIO_PTO   = '$horario'
                    WHERE ID_PUNTO='$id'");
        $obj = new geoJSON();
        $actualizar = $obj->crearTodos();
        return $consulta;
    }

    public function deletePuntos($id_punto)
    {
        require_once "geojson_modelo.php";
        $consulta =$this->db->query("DELETE FROM punto_verde WHERE ID_PUNTO='$id_punto'");
        $obj = new geoJSON();
        $actualizar = $obj->crearTodos();
    }

    public function showPunto($id_punto)
    {
        require_once "geojson_modelo.php";
        $consulta =$this->db->query("UPDATE punto_verde SET ESTADO = 'Habilitado' WHERE ID_PUNTO = '$id_punto'");
        $obj = new geoJSON();
        $actualizar = $obj->crearTodos();
    }

    public function hidePunto($id_punto)
    {
        require_once "geojson_modelo.php";
        $consulta=$this->db->query("UPDATE punto_verde SET ESTADO = 'Inhabilitado' WHERE ID_PUNTO = '$id_punto'");
        $obj = new geoJSON();
        $actualizar = $obj->crearTodos();
    }
}
