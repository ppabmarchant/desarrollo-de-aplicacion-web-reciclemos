<?php

class Reportes
{
    private $db;
    public $reportes;
    
    public function __construct()
    {
        require_once "conexion.php";
        $this->db  = Conectar::conexion();
        $this->reportes = array();
    }

    public function addReporte($ID_PUNTO, $DESCRIPCION)
    {
        $consulta = $this->db->query("INSERT INTO reportes VALUES (null, '$ID_PUNTO', '$DESCRIPCION')");
    }

    public function getReporte()
    {
        $consulta = $this->db->query("SELECT * FROM punto_verde, gestores, reportes WHERE punto_verde.ID_PUNTO = reportes.ID_PUNTO AND punto_verde.ID_GESTOR1 = gestores.ID_GESTOR");
        $this->reportes = $consulta;
        return $this->reportes;
    }

    public function getCantidadPuntosGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT COUNT(*) cantidad FROM punto_verde, gestores, reportes
                    WHERE reportes.ID_PUNTO = punto_verde.ID_PUNTO
                    AND punto_verde.ID_GESTOR1 = gestores.ID_GESTOR
                    AND gestores.NOMBRE_GESTOR = '$USERNAME_GESTOR'");
        $this->puntos = $consulta->fetch(PDO::FETCH_ASSOC);
        return $this->puntos;
    }

    public function getReporteGestor($USERNAME_GESTOR)
    {
        $consulta = $this->db->query("SELECT * FROM punto_verde, gestores, reportes
                    WHERE reportes.ID_PUNTO = punto_verde.ID_PUNTO
                    AND punto_verde.ID_GESTOR1 = gestores.ID_GESTOR
                    AND gestores.NOMBRE_GESTOR = '$USERNAME_GESTOR'");
        $this->reportes = $consulta;
        return $this->reportes;
    }

    public function deleteReporte($ID_PUNTO)
    {
        $consulta =$this->db->query("DELETE FROM reportes WHERE ID_PUNTO = '$ID_PUNTO'");
    }
}
