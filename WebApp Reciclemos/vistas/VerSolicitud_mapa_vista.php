<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$controladorSolicitudes = "MostrarId";
$ID_SOLICITUD = $_GET['ID_SOLICITUD'];
require_once "../controlador/solicitudes_controlador.php";
foreach ($arraySolicitud as $solicitud) {
?>
<br>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5">
    <form enctype="multipart/form-data" method="post" action="../controlador/solicitudes_controlador.php">
    <div class="row">
      <div class="col-25">
        <label for="fname">Nombre del Punto</label>
      </div>
      <div class="col-75">
        <input type="text" name="nombre" value="<?php echo $solicitud['NOMBRE_PTO']; ?>" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Dirección</label>
      </div>
      <div class="col-75">
        <input type="text" name="ubicacion" value="<?php echo $solicitud['UBICACION_PTO']; ?>"require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Latitud</label>
      </div>
      <div class="col-75">
        <input type="text" id="latitud" name="latitud" value="<?php echo $solicitud['LATITUD']; ?>" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Longitud</label>
      </div>
      <div class="col-75">
        <input type="text" id="longitud" name="longitud" value="<?php echo $solicitud['LONGITUD']; ?>" require_onced>
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="fname">Tipo de Instalación</label>
      </div>
      <div class="col-75">
        <select name="tipo_punto" require_onced>
      <option value="<?php echo $solicitud['TIPO_PUNTO'];?>"><?php echo $solicitud['TIPO_PUNTO'];?></option>
      <option>-----</option>
      <?php
      $controladorCategorias = "Mostrar";
      require_once "../controlador/categorias_controlador.php";
      foreach ($arrayCategorias as $key) {
        echo "<option value=" . $key['TIPO_PUNTO'].">" . $key['TIPO_PUNTO'] . "</option>";
      }
      ?>
      </select>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Horario de funcionamiento</label>
      </div>
      <div class="col-75">
        <input type="text" name="horario" value="<?php echo $solicitud['HORARIO_PTO']; ?>" require_onced>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Gestor del Punto Verde</label>
      </div>
      <div class="col-75">
        <select name="idGestor" require_onced>
        <option value="<?php echo $solicitud['ID_GESTOR'];?>"><?php echo $solicitud['NOMBRE_GESTOR'];?></option>
        <option>-----</option>
        <?php
        $controladorGestores = "Mostrar";
        require_once "../controlador/gestores_controlador.php";
        foreach ($arrayGestor as $key) {
          echo "<option value=" . $key['ID_GESTOR'].">" . $key['NOMBRE_GESTOR'] . "</option>";
        }
        ?>
        </select>
      </div>
    </div>
    <?php 
    }
    ?>
    <input type="hidden" name="controladorSolicitudes" value="Aprobar">
    <input type="hidden" name="ID_SOLICITUD" value="<?php echo $solicitud['ID_SOLICITUD']; ?>">
    <input type="submit" name="Agregar" value="Aprobar Punto Verde">
    </form>
    <form method="POST" action="../controlador/solicitudes_controlador.php">
      <input type="hidden" name="controladorSolicitudes" value="Rechazar">
      <input type="hidden" name="ID_SOLICITUD" value="<?php echo $solicitud['ID_SOLICITUD']; ?>">
      <input type="submit" class="btn btn-danger" value="Rechazar Punto Verde">
    </form>
    </div>
    <div class="col-md-7">
      <div id=mapid></div>
    </div>
  </div>
</div>
<script src="../mapa/mapa_solicitud.js"></script>
</body>
</html>
