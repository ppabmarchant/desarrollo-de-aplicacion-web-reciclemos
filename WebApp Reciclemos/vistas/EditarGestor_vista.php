<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$ID_GESTOR = $_POST['id_gestor'];
$controladorGestores = "MostrarPorId";
require_once "../controlador/gestores_controlador.php";
?>
<div class="container">
	<form action="../controlador/gestores_controlador.php" method="post">
    <div class="row">
    	<?php 
    	foreach ($arrayGestor as $gestores) {
    	?>
        <label>Nombre del Gestor</label>
        <input type="text" name="nombreGestor" value="<?php echo $gestores['NOMBRE_GESTOR']; ?>" require_onced>
        <label>Email de contacto</label>
        <input type="email" name="emailGestor" value="<?php echo $gestores['CONTACTO_GESTOR']; ?>" require_onced>
        <label>Contraseña</label>
        <input type="text" name="contrasenaGestor" value="<?php echo $gestores['PASSWORD_GESTOR']; ?>" require_onced>
        <?php 
        } ?>
      </div>
      <div class="row">
        <div class="col-75">
      <input type="hidden" name="controladorGestores" value="Editar">
      <input type="hidden" name="id_gestor" value="<?php echo $ID_GESTOR; ?>">
      <input type="submit" value="Guardar cambios">
      </div>
    </div>
      </form>
    </div>