<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
if (!isset($_POST['id_punto'])) {
  header("Location: VerPunto_vista.php");
}
require_once "Banner_admin.php";
$id_punto = $_POST['id_punto'];
$controladorPuntos = "VerPuntosId";
require_once "../controlador/puntos_controlador.php";
foreach ($arrayPuntos as $puntos) {
?>
<br>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5">
    <form enctype="multipart/form-data" method="post" action="../controlador/puntos_controlador.php">
    <div class="row">
      <div class="col-25">
        <label for="fname">Nombre del Punto</label>
      </div>
      <div class="col-75">
        <input type="text" name="nombre" value="<?php echo $puntos['NOMBRE_PTO']; ?>" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Dirección</label>
      </div>
      <div class="col-75">
        <input type="text" name="ubicacion" value="<?php echo $puntos['UBICACION_PTO']; ?>"require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Latitud</label>
      </div>
      <div class="col-75">
        <input type="text" id="latitud" name="latitud" value="<?php echo $puntos['LATITUD']; ?>" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Longitud</label>
      </div>
      <div class="col-75">
        <input type="text" id="longitud" name="longitud" value="<?php echo $puntos['LONGITUD']; ?>" require_onced>
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="fname">Tipo de Instalación</label>
      </div>
      <div class="col-75">
        <select name="tipo_punto" require_onced>
      <option value="<?php echo $puntos['TIPO_PUNTO'];?>"><?php echo $puntos['TIPO_PUNTO'];?></option>
      <option>-----</option>
      <?php
      $controladorCategorias = "Mostrar";
      require_once "../controlador/categorias_controlador.php";
      foreach ($arrayCategorias as $key) {
        echo "<option value=" . $key['TIPO_PUNTO'].">" . $key['TIPO_PUNTO'] . "</option>";
      }
      ?>
      </select>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Horario de funcionamiento</label>
      </div>
      <div class="col-75">
        <input type="text" name="horario" value="<?php echo $puntos['HORARIO_PTO']; ?>" require_onced>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Gestor del Punto Verde</label>
      </div>
      <div class="col-75">
        <select name="idGestor" require_onced>
        <option value="<?php echo $puntos['ID_GESTOR'];?>"><?php echo $puntos['NOMBRE_GESTOR'];?></option>
        <option>-----</option>
        <?php
        $controladorGestores = "Mostrar";
        require_once "../controlador/gestores_controlador.php";
        foreach ($arrayGestor as $key) {
          echo "<option value=" . $key['ID_GESTOR'].">" . $key['NOMBRE_GESTOR'] . "</option>";
        }
        ?>
        </select>
      </div>
    </div>
    <div class="row">
      <input type="hidden" name="controladorPuntos" value="EditarPuntos">
      <input type="hidden" name="id_punto" value="<?php echo $puntos['ID_PUNTO']; ?> ">
      <br><input type="submit" name="Registrar" value="Registrar">
    </div>
    <?php 
    }
    ?>
  </form>
    </div>
    <div class="col-md-7">
      <div id=mapid></div>
    </div>
  </div>
</div>
<script src="../mapa/mapa_edit.js"></script>
</body>
</html>
