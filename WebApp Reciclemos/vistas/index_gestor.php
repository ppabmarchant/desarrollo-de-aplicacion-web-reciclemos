<?php
session_start();
clearstatcache();
if (!isset($_SESSION['username_gestor'])) {
  header("location: ../index.php");
}
require_once "Banner_gestor.php";
$controladorPuntos      = "VerCantidadGestor";
$controladorReportes    = "CantidadReportesGestor";
$controladorSolicitudes = "CantidadPendientesByGestor";
$username_gestor = $_SESSION['username_gestor'];
require_once "../controlador/puntos_controlador.php";
require_once "../controlador/reportes_controlador.php";
require_once "../controlador/solicitudes_controlador.php";
?>
<script>
var username_gestor = "<?php echo $username_gestor; ?>";	
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<label>Bienvenido <?php echo $username_gestor; ?></label>
			<table class="table">
		  	<thead class="thead-dark">
			    <tr>
			      <th scope="col" colspan="2">Resumen</th>
			    </tr>
			 </thead>
			<tbody>
		    <tr>
		      	<td>Total de Puntos Verdes</td>
		      	<td><?php echo $cantidadPuntos['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Puntos Limpios</td>
		      	<td><?php echo $cantidadPuntoLimpio['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Campanas</td>
		      	<td><?php echo $cantidadCampanas['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Contenedores</td>
		      	<td><?php echo $cantidadContenedor['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Puntos Verdes deshabilitados</td>
		      	<td><?php echo $cantidadInhabilitados['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Reportes</td>
		      	<td><?php echo $cantidadReportes['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Solicitudes pendientes</td>
		      	<td><?php echo $cantidadPendientesByGestor['cantidad']; ?></td>
		    </tr>
			</tbody>
			</table>	
		</div>
		<div class="col-md-8">
			<br>
			<center><button type="button" id="todos" class="btn btn-danger">Todos</button>
	        <button type="button" id="PuntoLimpio" class="btn btn-primary">Punto Limpio</button>
	        <button type="button" id="campana" class="btn btn-success">Campana</button>
	        <button type="button" id="Contenedor" class="btn btn-warning">Contenedor</button>
			<div id=mapid>
			</div>
			</center>
		</div>
	</div>
</div>


<script src="../mapa/mapa_gestor.js"></script>
</body>
</html>