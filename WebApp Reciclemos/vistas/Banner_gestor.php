<!DOCTYPE html>
<html>
<head>
  <title>Panel de Control - Gestores</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
  <link href="estilos/estilos.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans" rel="stylesheet" type="text/css">
  <!--LEAFLET CSS-->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="crossorigin=""/>
  <!--LEAFLET javaScript-->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="   crossorigin=""></script>
  <!--Estilo Mapa-->
  <style>
    #mapid { height: 400px; }
  </style>
  <!-- CSS BOOTSTRAP -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <!--ARCHIVO GEOJSON-->
  <script src="../controlador/datosGeoJSON.js"></script>
</head>
<body>
  <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<div class="baner">
<center><img src="imagenes/pc_text.png" class="responsiv"></center>
</div>
<div class="topnav" id="myTopnav">
  <a href="index_gestor.php">Inicio</a>
  <a href="VerPunto_gestor_vista.php">Mis Puntos Verdes</a>
  <a href="VerSolicitudes_gestor_vista.php">Mis Solicitudes</a>
  <a href="Reportes_gestor_vista.php">Reportes ciudadanos</a>
  <a href="Solicitud_gestor_vista.php">Solicitud de Punto Verde</a>
  <a href="../controlador/login_controlador.php?loginControlador=Cerrar" class="float-right">Cerrar Sesión</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>