<?php
$id_punto = $_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>INICIO</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
	<link href="estilos/estilos.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans" rel="stylesheet" type="text/css">
	<!--LEAFLET CSS-->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="crossorigin=""/>
  <!--LEAFLET javaScript-->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="   crossorigin=""></script>
  <!--Estilo Mapa-->
    <style>
    	#mapid { height: 400px; }
    </style>
  <!-- CSS BOOTSTRAP -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<!--ARCHIVO GEOJSON-->
	<script src="../controlador/datosGeoJSON.js"></script>

</head>
<body>
<div class="baner">
<center><img src="imagenes/title.png" class="responsive"></center>
</div>
<div class="topnav" id="myTopnav">
  <a href="../index.php">Inicio</a>
  <a href="">Cómo reciclar</a>
  <a href="Login_vista.php" class="float-right">Acceso colaboradores</a>
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
</div>
<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
<script>
	var id_punto = "<?php echo $id_punto; ?>";
</script>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
		<label>Formulario para reportar problemas con un Punto Verde</label>
		<form method="POST" action="../controlador/reportes_controlador.php">
		<label>Describa la situación que desea reportar</label>
		<textarea  name="descripcion" maxlength="1000" required></textarea>
		<input type="hidden" name="controladorReportes" value="Agregar">
		<input type="hidden" name="id_punto" value="<?php echo $id_punto; ?>">
		<input type="submit" name="Enviar">
		</form>
		</div>
		<div class="col-md-8">
		<label>Punto Verde que está reportando</label>
			<div id=mapid></div>
		<script src="../mapa/mapa_reporte.js"></script>
		</div>
	</div>
</div>
</body>
</html>