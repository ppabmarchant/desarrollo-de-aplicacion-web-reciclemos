<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$NOMBRE_CATEGORIA = $_POST['tipo_punto'];
$controladorCategorias = "MostrarPorId";
require_once "../controlador/categorias_controlador.php";
?>
<div class="container">
	<form action="../controlador/categorias_controlador.php" method="post">
    <div class="row">
    	<?php 
    	foreach ($arrayCategorias as $categorias) {
    	?>
        <label>Nombre de la categoría</label>
        <input type="text" name="tipo_punto" value="<?php echo $categorias['TIPO_PUNTO']; ?>" require_onced>
        <label>Elementos que recibe</label>
        <input type="text" name="elementos_recibe" value="<?php echo $categorias['ELEMENTOS_RECIBE']; ?>" require_onced>
        <?php 
        } ?>
      </div>
      <div class="row">
        <div class="col-75">
      <input type="hidden" name="tipo_punto_inicial" value="<?php echo $categorias['TIPO_PUNTO']; ?>">
      <input type="hidden" name="controladorCategorias" value="Editar">
      <input type="submit" value="Guardar cambios">
      </div>
    </div>
      </form>
    </div>