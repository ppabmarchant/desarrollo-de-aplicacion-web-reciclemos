<?php
session_start();
clearstatcache();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$controladorPuntos      = "VerCantidad";
$controladorGestores    = "VerCantidad";
$controladorCategorias  = "VerCantidad";
$controladorSolicitudes = "CantidadPendientes";
require_once "../controlador/puntos_controlador.php";
require_once "../controlador/gestores_controlador.php";
require_once "../controlador/categorias_controlador.php";
require_once "../controlador/solicitudes_controlador.php";
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<br>
			<table class="table">
		  	<thead class="thead-dark">
			    <tr>
			      <th scope="col" colspan="2">Resumen</th>
			    </tr>
			 </thead>
			<tbody>
		    <tr>
		      	<td>Total de Puntos Verdes</td>
		      	<td><?php echo $cantidadPuntos['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Puntos Limpios</td>
		      	<td><?php echo $cantidadPuntoLimpio['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Campanas</td>
		      	<td><?php echo $cantidadCampanas['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de Contenedor</td>
		      	<td><?php echo $cantidadContenedor['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Puntos Verdes habilitados</td>
		      	<td><?php echo $cantidadHabilitados['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Puntos Verdes deshabilitados</td>
		      	<td><?php echo $cantidadInhabilitados['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Cantidad de gestores</td>
		      	<td><?php echo $cantidadGestores['cantidad']; ?></td>
		    </tr>
		    <tr>
		      	<td>Solicitudes pendientes</td>
		      	<td><?php echo $cantidadPendientes['cantidad']; ?></td>
		    </tr>
			</tbody>
			</table>	
		</div>
		<div class="col-md-8">
			<br>
			<center><button type="button" id="todos" class="btn btn-danger">Todos</button>
	        <button type="button" id="PuntoLimpio" class="btn btn-primary">Punto Limpio</button>
	        <button type="button" id="campana" class="btn btn-success">Campana</button>
	        <button type="button" id="Contenedor" class="btn btn-warning">Contenedor</button>
			<div id=mapid>
			</div>
			</center>
		</div>
	</div>
</div>


<script src="../mapa/mapa_admin.js"></script>
</body>
</html>
