<?php 
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$controladorReportes = "Mostrar";
require_once "../controlador/reportes_controlador.php";
?>
<a href="../controlador/geojson_controlador.php?controladorGeoJSON=Actualizar"  class="btn btn-warning btn-lg btn-block" ><i class="far fa-plus-square"></i>   Actualizar Mapa</a>
<div class="container-fluid">
	<div class="row">
			<table class="table">
		  	<thead class="thead-dark">
			    <tr>
			      <th scope="col">Nombre Punto</th>
			      <th scope="col">Ubicación</th>
			      <th scope="col">Descripción</th>
			      <th scope="col">Nombre gestor</th>
			      <th scope="col">Contacto gestor</th>
			      <th scope="col">Estado</th>
			      <th scope="col" colspan="3"><center>Acciones</center></th>
			    </tr>
			 </thead>
			<tbody>
		    <?php
			foreach ($arrayReportes as $reportes) {
			?>
		    <tr>
		      	<td><?php echo $reportes['NOMBRE_PTO']; ?></td>
		      	<td><?php echo $reportes['UBICACION_PTO']; ?></td>
		      	<td><?php echo $reportes['DESCRIPCION']; ?></td>
		      	<td><?php echo $reportes['NOMBRE_GESTOR']; ?></td>
		      	<td><?php echo $reportes['CONTACTO_GESTOR']; ?></td>
		      	<td><?php echo $reportes['ESTADO']; ?></td>
		      	<td>
	            <form action="../controlador/puntos_controlador.php" method="post">
	              <input type="hidden" name="controladorPuntos" value="Habilitar">
	              <input type="hidden" name="id_punto" value="<?php echo $reportes['ID_PUNTO'];?>">
	              <abbr title="Habilitar"><button><i class="fas fa-eye"></i></button></abbr>
	            </form>
	        	</td>
	        	<td>
	            <form action="../controlador/puntos_controlador.php" method="post">
	              <input type="hidden" name="controladorPuntos" value="Inhabilitar">
	              <input type="hidden" name="id_punto" value="<?php echo $reportes['ID_PUNTO'];?>">
	              <abbr title="Inhabilitar"><button><i class="fas fa-eye-slash"></i></button></abbr>
	            </form>
	        	</td>
	        	<td>
	            <form action="../controlador/reportes_controlador.php" method="post">
	              <input type="hidden" name="controladorReportes" value="Eliminar">
	              <input type="hidden" name="id_punto" value="<?php echo $reportes['ID_PUNTO'];?>">
	              <abbr title="Eliminar"><button><i class="fas fa-trash-alt"></i></button></abbr>
	            </form>
	        	</td>
	        	
		    </tr>
			<?php 
			}
			?>
			</tbody>
			</table>
	</div>
</div>


