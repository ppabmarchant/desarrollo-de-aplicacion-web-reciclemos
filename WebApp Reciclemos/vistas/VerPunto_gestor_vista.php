<?php
session_start();
clearstatcache();
if (!isset($_SESSION['username_gestor'])) {
  header("location: ../index.php");
}
require_once "Banner_gestor.php";
$controladorPuntos = "VerPuntosGestor";
$username_gestor = $_SESSION['username_gestor'];
require_once "../controlador/puntos_controlador.php";
?>
<div class="container-fluid">
  <div class="row">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"><center>Nombre</center></th>
      <th scope="col"><center>Dirección</center></th>
      <th scope="col"><center>Tipo de Instalación</center></th>
      <th scope="col"><center>Elementos que recibe</center></th>
      <th scope="col"><center>Estado</center></th>
      <th scope="col"><center>Horario</center></th>
      <th scope="col" colspan="4"><center>Acciones</center></th>
    </tr>
  </thead>

  <tbody>
      <?php
  foreach ($arrayPuntos as $puntos) {
  ?>
  <tr>
    <th scope="row"><?php echo $puntos['NOMBRE_PTO']; ?></th>
    <td><?php echo $puntos['UBICACION_PTO']; ?></td>
    <td><?php echo $puntos['TIPO_PUNTO']; ?></td>
    <td><?php echo $puntos['ELEMENTOS_RECIBE']; ?></td>
    <td><?php echo $puntos['ESTADO']; ?></td>
    <td><?php echo $puntos['HORARIO_PTO']; ?></td>
    <td><form action="../controlador/puntos_controlador.php" method="post">
      <input type="hidden" name="controladorPuntos" value="EliminarPuntos">
      <input type="hidden" name="id_punto" value="<?php echo $puntos['ID_PUNTO'];?>">
      <abbr title="Eliminar"><button><i class="fas fa-trash-alt"></i></button></abbr>
      </form>
    </td>
    <td>
      <form action="EditarPunto_gestor_vista.php" method="post">
        <input type="hidden" name="id_punto" value="<?php echo $puntos['ID_PUNTO'];?>">
        <abbr title="Editar"><button><i class="fas fa-user-edit"></i></button></abbr>
      </form>
    </td>
    <td>
      <form action="../controlador/puntos_controlador.php" method="post">
        <input type="hidden" name="id_punto" value="<?php echo $puntos['ID_PUNTO'];?>">
        <input type="hidden" name="controladorPuntos" value="Inhabilitar">
        <abbr title="Inhabilitar"><button><i class="fas fa-eye-slash"></i></button></abbr>
      </form>
    </td>
    </tr>
<?php 
}
?>
  </tbody>
</table>
</div>
</div>