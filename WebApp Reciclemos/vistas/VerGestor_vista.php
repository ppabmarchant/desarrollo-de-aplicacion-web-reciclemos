<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$controladorGestores = "Mostrar";
require_once "../controlador/gestores_controlador.php";
?>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-8">
      <br>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Contacto</th>
            <th scope="col" colspan="2">Acciones</th>
          </tr>
       </thead>
      <tbody>
        <?php
      foreach ($arrayGestor as $gestor) {
      ?>
        <tr>
          <td><?php echo $gestor['NOMBRE_GESTOR']; ?></td>
          <td><?php echo $gestor['CONTACTO_GESTOR']; ?></td>
          <td>
            <form action="../controlador/gestores_controlador.php" method="post">
              <input type="hidden" name="controladorGestores" value="EliminarGestor">
              <input type="hidden" name="id_gestor" value="<?php echo $gestor['ID_GESTOR'];?>">
              <abbr title="Eliminar"><button><i class="fas fa-trash-alt"></i></button></abbr>
            </form>
          </td>
          <td>
            <form action="EditarGestor_vista.php" method="post">
              <input type="hidden" name="id_gestor" value="<?php echo $gestor['ID_GESTOR'];?>">
              <abbr title="Editar"><button><i class="fas fa-user-edit"></i></button></abbr>
            </form>
          </td>
        </tr>
      <?php 
      }
      ?>
      </tbody>
      </table>
    </div>
  <div class="col-md-4">
    <br>
    <div class="container">
    <form action="../controlador/gestores_controlador.php" method="post">
    <div class="row">
        <label>Nombre del gestor</label>
        <input type="text" name="nombreGestor" required>
        <label>Email de contacto</label>
        <input type="email" name="emailGestor" required>
        <label>Contraseña</label>
        <input type="text" name="contrasenaGestor" required>
      </div>
      <div class="row">
        <div class="col-75">
      <input type="hidden" name="controladorGestores" value="Agregar">
      <input type="submit" value="Registrar">
      </div>
    </div>
      </form>
    </div>
  </div>
  </div>
</div>

