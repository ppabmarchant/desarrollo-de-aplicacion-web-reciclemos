<?php
session_start();
if (!isset($_SESSION['username_admin'])){
  header("location: ../index.php");
}
require_once "Banner_admin.php";
?>
<body>
<br>
  <?php
if (isset($notificacion)) {
  echo $notificacion;
}
  ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5">
      <form enctype="multipart/form-data" method="post" action="../controlador/puntos_controlador.php">

    <div class="row">
      <div class="col-25">
        <label for="fname">Nombre del Punto</label>
      </div>
      <div class="col-75">
        <input type="text" name="nombre" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Dirección</label>
      </div>
      <div class="col-75">
        <input type="text" name="ubicacion" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Latitud</label>
      </div>
      <div class="col-75">
        <input type="text" name="latitud" id="latitud" require_onced>
      </div>
    </div>
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Longitud</label>
      </div>
      <div class="col-75">
        <input type="text" name="longitud" id="longitud" require_onced>
      </div>
    </div>

     <div class="row">
      <div class="col-25">
        <label for="fname">Tipo de Instalación</label>
      </div>
      <div class="col-75">
        <select name="tipo_punto" require_onced>
          <option value="">Selecione el tipo de punto</option>
        <?php
        $controladorCategorias = "Mostrar";
        require_once "../controlador/categorias_controlador.php";
        foreach ($arrayCategorias as $key) {
          echo "<option value=" . $key['TIPO_PUNTO'].">" . $key['TIPO_PUNTO'] . "</option>";
        }
        ?>
      </select>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Horario de funcionamiento</label>
      </div>
      <div class="col-75">
        <input type="text" name="horario" require_onced>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">Gestor del Punto Verde</label>
      </div>
      <div class="col-75">
        <select name="idGestor" require_onced>
      <option value="">Selecione un gestor</option>
      <?php
      $controladorGestores = "Mostrar";
      require_once "../controlador/gestores_controlador.php";
      foreach ($arrayGestor as $key) {
        echo "<option value=" . $key['ID_GESTOR'].">" . $key['NOMBRE_GESTOR'] . "</option>";
      }
      ?>
      </select>
      </div>
    </div>
    <div class="row">
      <input type="hidden" name="controladorPuntos" value="AgregarPuntos">
      <br><input type="submit" name="Registrar" value="Registar">
    </div>
  </form>
    </div>

    <div class="col-md-7">
      <div id=mapid></div>
    </div>
  </div>
</div>

<script src="../mapa/mapa_agregar.js"></script>
</body>
</html>
