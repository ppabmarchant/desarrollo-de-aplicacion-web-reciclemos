<?php
session_start();
clearstatcache();
if (!isset($_SESSION['username_gestor'])) {
  header("location: ../index.php");
}
require_once "Banner_gestor.php";
$controladorSolicitudes = "MostrarGestor";
$ID_GESTOR = $_SESSION['id_gestor']; 
require_once "../controlador/solicitudes_controlador.php";
?>
<div class="container-fluid">
	<div class="row">
			<table class="table">
        <thead class="thead-dark">
          <tr>
          	<th scope="col">Id</th>
		    <th scope="col"><center>Nombre</center></th>
		    <th scope="col"><center>Dirección</center></th>
		    <th scope="col"><center>Tipo de Instalación</center></th>
		    <th scope="col"><center>Horario</center></th>
		    <th scope="col"><center>Estado</center></th>
		    <th scope="col" colspan="4"><center>Acciones</center></th>
          </tr>
       </thead>
      <tbody>
        <?php
      foreach ($arraySolicitud as $solicitud) {
      ?>
        <tr>
          <td><?php echo $solicitud['ID_SOLICITUD']; ?></td>
          <td><?php echo $solicitud['NOMBRE_PTO']; ?></td>
    		  <td><?php echo $solicitud['UBICACION_PTO']; ?></td>
    		  <td><?php echo $solicitud['TIPO_PTO1']; ?></td>
    		  <td><?php echo $solicitud['HORARIO_PTO']; ?></td>
    		  <td><?php echo $solicitud['ESTADO']; ?></td>
          <td>
            <abbr title="Ver detalles"><button><a href="VerSolicitud_gestor_mapa_vista.php?ID_SOLICITUD=<?php echo $solicitud['ID_SOLICITUD'];?>"><i class="fas fa-search"></i></a></button></abbr>
          </td>
          <td>
            <form action="../controlador/solicitudes_controlador.php" method="post">
              <input type="hidden" name="controladorSolicitudes" value="Eliminar">
              <input type="hidden" name="ID_SOLICITUD" value="<?php echo $solicitud['ID_SOLICITUD'];?>">
              <abbr title="Eliminar"><button><i class="fas fa-trash-alt"></i></button></abbr>
            </form>
          </td>
        </tr>
      <?php 
      }
      ?>
      </tbody>
      </table>
	</div>
</div>
</body>