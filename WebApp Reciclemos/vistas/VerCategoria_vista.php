<?php
session_start();
if (!isset($_SESSION['username_admin'])) {
  header("location: ../index.php");
}
require_once "Banner_admin.php";
$controladorCategorias = "Mostrar";
require_once "../controlador/categorias_controlador.php";
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<br>
			<table class="table">
		  	<thead class="thead-dark">
			    <tr>
			      <th scope="col">Tipo de Instalación</th>
			      <th scope="col">Elementos que recibe</th>
			      <th scope="col" colspan="2">Acciones</th>
			    </tr>
			 </thead>
			<tbody>
		    <?php
			foreach ($arrayCategorias as $categorias) {
			?>
		    <tr>
		      	<td><?php echo $categorias['TIPO_PUNTO']; ?></td>
		      	<td><?php echo $categorias['ELEMENTOS_RECIBE']; ?></td>
		      	<td>
	            <form action="../controlador/categorias_controlador.php" method="post">
	              <input type="hidden" name="controladorCategorias" value="Eliminar">
	              <input type="hidden" name="tipo_punto" value="<?php echo $categorias['TIPO_PUNTO'];?>">
	              <abbr title="Eliminar"><button><i class="fas fa-trash-alt"></i></button></abbr>
	            </form>
	        	</td>
	        	<td>
	            <form action="EditarCategoria_vista.php" method="post">
	              <input type="hidden" name="tipo_punto" value="<?php echo $categorias['TIPO_PUNTO'];?>">
	              <abbr title="Editar"><button><i class="fas fa-user-edit"></i></button></abbr>
	            </form>
          </td>
		    </tr>
			<?php 
			}
			?>
			</tbody>
			</table>
		</div>
	<div class="col-md-6">
		<br>
		<div class="container">
		<form action="../controlador/categorias_controlador.php" method="post">
		<div class="row">
	        <label>Tipo de Punto</label>
		    <input type="text" name="tipo_punto" require_onced>
	        <label>Elementos que recibe</label>
		    <input type="text" name="elementos_recibe" require_onced>
		</div>
		<div class="row">
			<div class="col-75">
		<input type="hidden" name="controladorCategorias" value="Agregar">
     	<input type="submit" value="Crear">
    	</div>
    </div>
		  </form>
		</div>
	</div>
	</div>
</div>

