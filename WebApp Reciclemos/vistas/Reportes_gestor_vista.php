<?php
session_start();
clearstatcache();
if (!isset($_SESSION['username_gestor'])) {
  header("location: ../index.php");
}
require_once "Banner_gestor.php";
$username_gestor = $_SESSION['username_gestor'];
$controladorReportes = "MostrarGestor";
require_once "../controlador/reportes_controlador.php";
?>
<div class="container-fluid">
	<div class="row">
			<table class="table">
		  	<thead class="thead-dark">
			    <tr>
			      <th scope="col">Nombre Punto</th>
			      <th scope="col">Ubicación</th>
			      <th scope="col">Tipo de Instalación</th>
			      <th scope="col">Descripción</th>
			      <th scope="col">Estado</th>
			    </tr>
			 </thead>
			<tbody>
		    <?php
			foreach ($arrayReportes as $reportes) {
			?>
		    <tr>
		      	<td><?php echo $reportes['NOMBRE_PTO']; ?></td>
		      	<td><?php echo $reportes['UBICACION_PTO']; ?></td>
		      	<td><?php echo $reportes['TIPO_PTO1']; ?></td>
		      	<td><?php echo $reportes['DESCRIPCION']; ?></td>
		      	<td><?php echo $reportes['ESTADO']; ?></td>
		    </tr>
			<?php 
			}
			?>
			</tbody>
			</table>
	</div>
</div>


