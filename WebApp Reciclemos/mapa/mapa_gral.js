//constantes
const MAP_IMAGE_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
const MAX_BASE_ZOOM = 17;
const ATTRUBTION_FOOTER = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

//Inicia el objeto Mapa
var mapa = L.map('mapid').setView([-37.47076836275045, -72.35175721157293], 18);


//Instancia la capa base para el mapa
L.tileLayer(MAP_IMAGE_URL, {attribution: ATTRUBTION_FOOTER,	maxZoom: MAX_BASE_ZOOM}).addTo(mapa);

//Se define la capa Contenedor junto a su marcador
var iconoContenedor = L.icon({
	iconUrl: 'vistas/imagenes/Contenedor.png',
	iconSize: [38, 38],
	iconAnchor: [22, 22],
	popupAnchor: [0, -30],
});
var capaContenedor = L.geoJSON(datosGeoJSON, {
    filter: function(feature, layer) {
        return feature.properties.TipodePunto== "Contenedor" && feature.properties.Estado=="Habilitado";
    },
    pointToLayer: function(feature, latlng) {
		return L.marker(latlng, {icon: iconoContenedor}).bindPopup("<h4>"+feature.properties.Nombre+
			"</h4><p><strong>Tipo de Punto Verde : </strong>"+feature.properties.TipodePunto+
			"<br><strong>Ubicación : </strong>"+feature.properties.Ubicacion+
			"<br><strong>Elementos que recibe : </strong>"+feature.properties.Elementosquerecibe+ 
			"<br><strong>Horario : </strong>"+feature.properties.Horario+
			"<br><strong>Gestor : </strong>"+feature.properties.Contacto+
			"<br><strong>Contacto : </strong>"+feature.properties.Emaildecontacto+
			"<br><a href='vistas/Reportar.php?id="+feature.properties.Id_Punto+"'>Reportar un problema</a></p>").openPopup();
		}
});
//Se define la capa Campana junto a su marcador
var iconoCampana = L.icon({
	iconUrl: 'vistas/imagenes/campana.png',
	iconSize: [48, 48],
	iconAnchor: [22, 22],
	popupAnchor: [0, -30],
});
var capaCampana = L.geoJSON(datosGeoJSON, {
    filter: function(feature, layer) {
        return feature.properties.TipodePunto== "Campana" && feature.properties.Estado=="Habilitado";
    },
    pointToLayer: function(feature, latlng) {
		return L.marker(latlng, {icon: iconoCampana}).bindPopup("<h4>"+feature.properties.Nombre+
			"</h4><p><strong>Tipo de Punto Verde : </strong>"+feature.properties.TipodePunto+
			"<br><strong>Ubicación : </strong>"+feature.properties.Ubicacion+
			"<br><strong>Elementos que recibe : </strong>"+feature.properties.Elementosquerecibe+ 
			"<br><strong>Horario : </strong>"+feature.properties.Horario+
			"<br><strong>Gestor : </strong>"+feature.properties.Contacto+
			"<br><strong>Contacto : </strong>"+feature.properties.Emaildecontacto+
			"<br><a href='vistas/Reportar.php?id="+feature.properties.Id_Punto+"'>Reportar un problema</a></p>").openPopup();
		}
});
//Se define la capa Punto Limpio junto a su marcador
var iconoPuntoLimpio = L.icon({
	iconUrl: 'vistas/imagenes/PuntoLimpio.png',
	iconSize: [48, 48],
	iconAnchor: [22, 22],
	popupAnchor: [0, -30],
});
var capaPuntoLimpio = L.geoJSON(datosGeoJSON, {
    filter: function(feature, layer) {
        return feature.properties.TipodePunto== "PuntoLimpio" && feature.properties.Estado=="Habilitado";
    },
    pointToLayer: function(feature, latlng) {
		return L.marker(latlng, {icon: iconoPuntoLimpio}).bindPopup("<h4>"+feature.properties.Nombre+
			"</h4><p><strong>Tipo de Punto Verde : </strong>"+feature.properties.TipodePunto+
			"<br><strong>Ubicación : </strong>"+feature.properties.Ubicacion+
			"<br><strong>Elementos que recibe : </strong>"+feature.properties.Elementosquerecibe+ 
			"<br><strong>Horario : </strong>"+feature.properties.Horario+
			"<br><strong>Gestor : </strong>"+feature.properties.Contacto+
			"<br><strong>Contacto : </strong>"+feature.properties.Emaildecontacto+
			"<br><a href='vistas/Reportar.php?id="+feature.properties.Id_Punto+"'>Reportar un problema</a></p>").openPopup();
		}
});

	var grupoCapas = L.layerGroup([capaCampana,capaContenedor,capaPuntoLimpio]).addTo(mapa);
	
	function agregarCapaCampana(){
		mapa.removeLayer(capaPuntoLimpio);
		mapa.removeLayer(capaContenedor);
		mapa.addLayer(capaCampana);
	}
	function agregarCapaPuntoLimpio(){
		mapa.removeLayer(capaCampana);
		mapa.removeLayer(capaContenedor);
		mapa.addLayer(capaPuntoLimpio);
	}
	function agregarCapaContenedor(){
		mapa.removeLayer(capaCampana);
		mapa.removeLayer(capaPuntoLimpio);
		mapa.addLayer(capaContenedor);
	}
	
	function agregarTodasCapas(){
		mapa.removeLayer(grupoCapas);
		mapa.addLayer(grupoCapas);
	}
	document.getElementById("campana").addEventListener("click", agregarCapaCampana);
	document.getElementById("PuntoLimpio").addEventListener("click", agregarCapaPuntoLimpio);
	document.getElementById("Contenedor").addEventListener("click", agregarCapaContenedor);
	document.getElementById("todos").addEventListener("click",agregarTodasCapas);
