//constantes
const MAP_IMAGE_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
const MAX_BASE_ZOOM = 18;
const DEFAULT_ZOOM_LEVEL = 15;
const ATTRUBTION_FOOTER = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; <a href="http://cloudmade.com">CloudMade</a>';


//Inicia el objeto Mapa
var mapa = L.map('mapid').setView([-37.47076836275045, -72.35175721157293], 14);

//Instancia la capa base para el mapa
L.tileLayer(MAP_IMAGE_URL, {attribution: ATTRUBTION_FOOTER,	maxZoom: MAX_BASE_ZOOM}).addTo(mapa);
//Fija la vista inicial del mapa en la posicion del dispositivo

//Se define la capa  junto a su marcador
var capa = L.geoJSON(datosGeoJSON, {
    filter: function(feature, layer) {
        return feature.properties.Id_Punto== id_punto;
    },
    pointToLayer: function(feature, latlng) {
		return L.marker(latlng).bindPopup("<h4>"+feature.properties.Nombre+
			"</h4><p><strong>Tipo de Punto Verde : </strong>"+feature.properties.TipodePunto+
			"<br><strong>Ubicación : </strong>"+feature.properties.Ubicacion+
			"<br><strong>Elementos que recibe : </strong>"+feature.properties.Elementosquerecibe+ 
			"<br><strong>Horario : </strong>"+feature.properties.Horario+
			"<br><strong>Gestor : </strong>"+feature.properties.Contacto+
			"<br><strong>Contacto : </strong>"+feature.properties.Emaildecontacto+"</p>").openPopup();
		}
}).addTo(mapa);