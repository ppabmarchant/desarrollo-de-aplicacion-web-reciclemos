const MAP_IMAGE_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
const MAX_BASE_ZOOM = 18;
const DEFAULT_ZOOM_LEVEL = 15;
const ATTRUBTION_FOOTER = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; <a href="http://cloudmade.com">CloudMade</a>';

//Ubicacion del marcador
var posicion = [document.getElementById('latitud').value,document.getElementById('longitud').value];

//Inicia el objeto Mapa
var mapa = L.map('mapid').setView(posicion,15);

//Instancia la capa base para el mapa
L.tileLayer(MAP_IMAGE_URL, {attribution: ATTRUBTION_FOOTER,	maxZoom: MAX_BASE_ZOOM}).addTo(mapa);


//Se define la capa Rejilla junto a su marcador
var latSolicitud = document.getElementById('latitud').value;
var lonSolicitud = document.getElementById('longitud').value;
var marker = L.marker([latSolicitud,lonSolicitud],{draggable: true})
			.bindPopup("")
			.addTo(mapa);

marker.on('dragend', function (e) {
  document.getElementById('latitud').value = marker.getLatLng().lat;
  document.getElementById('longitud').value = marker.getLatLng().lng;
});