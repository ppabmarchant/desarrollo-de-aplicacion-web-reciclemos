<?php
require_once "../modelos/reportes_modelo.php";
if (isset($_POST['controladorReportes'])) {
    $controladorReportes = $_POST['controladorReportes'];
}

switch ($controladorReportes) {
    case 'Mostrar':
        $obj = new Reportes();
        $arrayReportes = $obj->getReporte();
    break;
    case 'MostrarGestor':
        $obj = new Reportes();
        $arrayReportes = $obj->getReporteGestor($username_gestor);
    break;
    case 'CantidadReportesGestor':
        $obj = new Reportes();
        $cantidadReportes = $obj->getCantidadPuntosGestor($username_gestor);
    break;
    case 'Agregar':
        $id_punto    = $_POST['id_punto'];
        $descripcion = $_POST['descripcion'];
        $obj = new Reportes();
        $agregar= $obj->addReporte($id_punto, $descripcion);
        header("location: ../index.php");
    break;
    case 'Eliminar':
        $id_punto = $_POST['id_punto'];
        $obj = new Reportes();
        $eliminar = $obj->deleteReporte($id_punto);
        header("location: ../vistas/Reportes_vista.php");
    break;
}
