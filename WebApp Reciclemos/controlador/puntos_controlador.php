<?php
require_once "../modelos/puntos_modelo.php";
if (isset($_POST['controladorPuntos'])) {
    $controladorPuntos = $_POST['controladorPuntos'];
}
switch ($controladorPuntos) {
    case 'VerCantidad':
        $puntos = new Puntos();
        $cantidadPuntos = $puntos->getCantidadPuntos();
        $cantidadHabilitados = $puntos->getCantidadPuntosHabilitados();
        $cantidadInhabilitados = $puntos->getCantidadPuntosInhabilitados();
        $cantidadPuntoLimpio = $puntos->getCantidadPuntoLimpio();
        $cantidadCampanas = $puntos->getCantidadCampana();
        $cantidadContenedor = $puntos->getCantidadContenedor();
    break;
    case 'VerCantidadGestor':
        $puntos = new Puntos();
        $cantidadPuntos = $puntos->getCantidadPuntosGestor($username_gestor);
        $cantidadHabilitados = $puntos->getCantidadPuntosHabilitadosGestor($username_gestor);
        $cantidadInhabilitados = $puntos->getCantidadPuntosInhabilitadosGestor($username_gestor);
        $cantidadPuntoLimpio = $puntos->getCantidadPuntoLimpioGestor($username_gestor);
        $cantidadCampanas = $puntos->getCantidadCampanaGestor($username_gestor);
        $cantidadContenedor = $puntos->getCantidadContenedorGestor($username_gestor);
    break;
    case 'VerPuntos':
        $puntos      = new Puntos();
        $arrayPuntos = $puntos->getPuntos();
    break;
    case 'VerPuntosId':
        $puntos      = new Puntos();
        $arrayPuntos = $puntos->getPuntosPorId($id_punto);
    break;
    case 'VerPuntosGestor':
        $puntos = new Puntos();
        $arrayPuntos = $puntos->getPuntosPorGestor($username_gestor);
    break;
    case 'AgregarPuntos':
        //recuperar variables POST
        $nombre      = $_POST['nombre'];
        $ubicacion   = $_POST['ubicacion'];
        $latitud     = $_POST['latitud'];
        $longitud    = $_POST['longitud'];
        $tipoPunto   = $_POST['tipo_punto'];
        $idGestor    = $_POST['idGestor'];
        $horario     = $_POST['horario'];
        $puntos = new Puntos();
        $agregarPunto = $puntos->addPuntos($nombre, $ubicacion, $latitud, $longitud, $tipoPunto, $idGestor, $horario);
        header("location: ../vistas/VerPunto_vista.php");
    break;
    case 'EditarPuntos':
        //recuperar variables POST
        $id_punto    = $_POST['id_punto'];
        $nombre      = $_POST['nombre'];
        $ubicacion   = $_POST['ubicacion'];
        $latitud     = $_POST['latitud'];
        $longitud    = $_POST['longitud'];
        $tipoPunto   = $_POST['tipo_punto'];
        $idGestor    = $_POST['idGestor'];
        $horario     = $_POST['horario'];
        $puntos = new Puntos();
        $eliminar = $puntos->updatePuntos($id_punto, $nombre, $ubicacion, $latitud, $longitud, $tipoPunto, $idGestor, $horario);
        header("Location:".$_SERVER['HTTP_REFERER']);
    break;
    case 'EliminarPuntos':
        //recuperar variables POST
        $id_punto = $_POST['id_punto'];
        $puntos = new Puntos();
        $eliminar = $puntos->deletePuntos($id_punto);
        header("Location:".$_SERVER['HTTP_REFERER']);
    break;
    case 'Habilitar':
        $id_punto = $_POST['id_punto'];
        $punto = new Puntos();
        $habilitar = $punto->showPunto($id_punto);
        header("Location:".$_SERVER['HTTP_REFERER']);
    break;
    case 'Inhabilitar':
        $id_punto = $_POST['id_punto'];
        $punto = new Puntos();
        $inhablitar = $punto->hidePunto($id_punto);
        header("Location:".$_SERVER['HTTP_REFERER']);
    break;
}
