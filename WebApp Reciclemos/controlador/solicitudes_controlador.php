<?php
require_once "../modelos/solicitudes_modelo.php";
if (isset($_POST['controladorSolicitudes'])) {
    $controladorSolicitudes = $_POST['controladorSolicitudes'];
}
if (isset($_GET['controladorSolicitudes'])) {
    $controladorSolicitudes = $_GET['controladorSolicitudes'];
}
if (isset($_POST['ID_SOLICITUD'])) {
    $ID_SOLICITUD = $_POST['ID_SOLICITUD'];
}
if (isset($_GET['ID_SOLICITUD'])) {
    $ID_SOLICITUD = $_GET['ID_SOLICITUD'];
}

switch ($controladorSolicitudes) {
    case 'Agregar':
        $NOMBRE_PTO    = $_POST['nombre'];
        $UBICACION_PTO = $_POST['ubicacion'];
        $LATITUD       = $_POST['latitud'];
        $LONGITUD      = $_POST['longitud'];
        $TIPO_PTO      = $_POST['tipo_punto'];
        $ID_GESTOR     = $_POST['idGestor'];
        $HORARIO_PTO   = $_POST['horario'];
        $obj = new Solicitud();
        $agregar =$obj->add($NOMBRE_PTO, $UBICACION_PTO, $LATITUD, $LONGITUD, $TIPO_PTO, $ID_GESTOR, $HORARIO_PTO);
        $mensaje = "mensaje";
        header("Location: ../vistas/VerSolicitudes_gestor_vista.php");
    break;
    case 'Mostrar':
        $obj = new Solicitud();
        $arraySolicitud = $obj->get();
    break;
    case 'MostrarGestor':
        $obj = new Solicitud();
        $arraySolicitud = $obj->getByGestor($ID_GESTOR);
    break;
    case 'MostrarId':
        $obj = new Solicitud();
        $arraySolicitud = $obj->getById($ID_SOLICITUD);
    break;
    case 'Cantidad':
        $obj = new Solicitud();
        $cantidad = $obj->count();
    break;
    case 'CantidadGestor':
        $obj = new Solicitud();
        $cantidad = $obj->countByGestor($ID_GESTOR);
    break;
    case 'CantidadPendientes':
        $obj = new Solicitud();
        $cantidadPendientes = $obj->countPendientes();
    break;
    case 'CantidadPendientesByGestor':
        $obj = new Solicitud();
        $cantidadPendientesByGestor = $obj->countPendientesByGestor($ID_GESTOR);
        break;
    case 'Aprobar':
        require_once "puntos_controlador.php";
        $obj = new Solicitud();
        $aprobar = $obj->approve($ID_SOLICITUD);
        $obj2 = new Puntos();
        $nombre      = $_POST['nombre'];
        $ubicacion   = $_POST['ubicacion'];
        $latitud     = $_POST['latitud'];
        $longitud    = $_POST['longitud'];
        $tipoPunto   = $_POST['tipo_punto'];
        $idGestor    = $_POST['idGestor'];
        $horario     = $_POST['horario'];
        $agregar = $obj2->addPuntos($nombre, $ubicacion, $latitud, $longitud, $tipoPunto, $idGestor, $horario);
        header("Location: ../vistas/VerSolicitud_vista.php");
    break;
    case 'Rechazar':
        $obj = new Solicitud();
        $rechazar = $obj->refuse($ID_SOLICITUD);
        header("Location: ../vistas/VerSolicitud_vista.php");
    break;
    case 'Eliminar':
        $obj = new Solicitud();
        $eliminar = $obj->delete($ID_SOLICITUD);
        header("Location:".$_SERVER['HTTP_REFERER']);
    break;
}
