<?php
require_once "../modelos/categorias_modelo.php";
if (isset($_POST['controladorCategorias'])) {
    $controladorCategorias = $_POST['controladorCategorias'];
}
switch ($controladorCategorias) {
    case 'Mostrar':
        $categorias = new Categoria();
        $arrayCategorias = $categorias->getCategoria();
    break;
    case 'VerCantidad':
        $categorias = new Categoria();
        $cantidadCategorias = $categorias->getCantidadCategoria();
    break;
    case 'MostrarPorId':
        $NOMBRE_CATEGORIA = $_POST['tipo_punto'];
        $categorias = new Categoria();
        $arrayCategorias = $categorias->getCategoriaPorId($NOMBRE_CATEGORIA);
    break;
    case 'Agregar':
        $TIPO_PUNTO       = $_POST['tipo_punto'];
        $ELEMENTOS_RECIBE = $_POST['elementos_recibe'];
        $categorias = new Categoria();
        $agregar = $categorias->addCategoria($TIPO_PUNTO, $ELEMENTOS_RECIBE);
        header("location: ../vistas/VerCategoria_vista.php");
	break;
    case 'Eliminar':
        $TIPO_PUNTO = $_POST['tipo_punto'];
        $categorias = new Categoria();
        $eliminar = $categorias->deleteCategoría($TIPO_PUNTO);
        header("location: ../vistas/VerCategoria_vista.php");
    break;
    case 'Editar':
        $TIPO_PUNTO         = $_POST['tipo_punto'];
        $TIPO_PUNTO_INICIAL = $_POST['tipo_punto_inicial'];
        $ELEMENTOS_RECIBE   = $_POST['elementos_recibe'];
        $categorias = new Categoria();
        $editar = $categorias->updateCategoria($TIPO_PUNTO, $TIPO_PUNTO_INICIAL, $ELEMENTOS_RECIBE);
        header("location: ../vistas/VerCategoria_vista.php");
    break;
    default:
    break;
}
