<?php
require_once "../modelos/gestores_modelo.php";
if (isset($_POST['controladorGestores'])) {
    $controladorGestores = $_POST['controladorGestores'];
}
switch ($controladorGestores) {
    case 'Mostrar':
        $gestor = new Gestor();
        $arrayGestor = $gestor->getGestor();
    break;
    case 'VerCantidad':
        $gestor = new Gestor();
        $cantidadGestores = $gestor->getCantidadGestor();
    break;
    case 'MostrarPorId':
        $gestor = new Gestor();
        $arrayGestor = $gestor->getGestorPorId($ID_GESTOR);
    break;
    case 'Agregar':
        $NOMBRE_GESTOR     = $_POST['nombreGestor'];
        $CONTACTO_GESTOR   = $_POST['emailGestor'];
        $CONTRASENA_GESTOR = $_POST['contrasenaGestor'];
        $gestor = new Gestor();
        $agregarGestor = $gestor->addGestor($NOMBRE_GESTOR, $CONTACTO_GESTOR, $CONTRASENA_GESTOR);
        header("location: ../vistas/VerGestor_vista.php");
    break;
    case 'Editar':
        $ID_GESTOR         = $_POST['id_gestor'];
        $NOMBRE_GESTOR     = $_POST['nombreGestor'];
        $CONTACTO_GESTOR   = $_POST['emailGestor'];
        $CONTRASENA_GESTOR = $_POST['contrasenaGestor'];
        $gestor= new Gestor();
        $editar = $gestor->updateGestor($ID_GESTOR, $NOMBRE_GESTOR, $CONTACTO_GESTOR, $CONTRASENA_GESTOR);
        header("location: ../vistas/VerGestor_vista.php");
    break;
    case 'EliminarGestor':
        $ID_GESTOR = $_POST['id_gestor'];
        $gestor = new Gestor();
        $eliminar = $gestor->deleteGestor($ID_GESTOR);
        header("location: ../vistas/VerGestor_vista.php");
    break;
    default:
    break;
}
