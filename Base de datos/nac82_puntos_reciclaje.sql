-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-09-2021 a las 13:34:19
-- Versión del servidor: 10.1.45-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nac82_puntos_reciclaje`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `ID_ADMIN` int(11) NOT NULL,
  `USER_ADMIN` varchar(25) NOT NULL,
  `PASSWORD_ADMIN` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`ID_ADMIN`, `USER_ADMIN`, `PASSWORD_ADMIN`) VALUES
(1, 'admin', 'A12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `TIPO_PUNTO` varchar(15) NOT NULL,
  `ELEMENTOS_RECIBE` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contiene la definición de las categorías de puntos limpios y puntos verdes';

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`TIPO_PUNTO`, `ELEMENTOS_RECIBE`) VALUES
('Campana', 'Botellas de vidrio'),
('Contenedor', 'Botellas y envases de material tipo PET 1.'),
('PuntoLimpio', 'Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestores`
--

CREATE TABLE `gestores` (
  `ID_GESTOR` int(11) NOT NULL,
  `NOMBRE_GESTOR` varchar(100) NOT NULL,
  `CONTACTO_GESTOR` varchar(300) NOT NULL,
  `PASSWORD_GESTOR` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contiene los datos de los gestores de residuos y las credenciades de acceso al administrador gestor de residuos';

--
-- Volcado de datos para la tabla `gestores`
--

INSERT INTO `gestores` (`ID_GESTOR`, `NOMBRE_GESTOR`, `CONTACTO_GESTOR`, `PASSWORD_GESTOR`) VALUES
(27, 'MyF', 'correo_myf@gmail.com', 'G12345'),
(29, 'Greenwalk', 'correo_greenwalk@gmail.com', 'G12345'),
(30, 'Cristororeciclaje', 'correo_cristororeciclaje@gmail.com', 'G12345'),
(31, 'Demarco', 'correo_demarco@gmail.com', 'G12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto_verde`
--

CREATE TABLE `punto_verde` (
  `ID_PUNTO` int(8) NOT NULL,
  `NOMBRE_PTO` varchar(200) NOT NULL,
  `UBICACION_PTO` varchar(200) NOT NULL,
  `LATITUD` float NOT NULL,
  `LONGITUD` float NOT NULL,
  `TIPO_PTO1` varchar(15) NOT NULL,
  `ID_GESTOR1` int(11) NOT NULL,
  `HORARIO_PTO` varchar(300) NOT NULL,
  `ESTADO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contiene los registros de puntos verdes y puntos limpios';

--
-- Volcado de datos para la tabla `punto_verde`
--

INSERT INTO `punto_verde` (`ID_PUNTO`, `NOMBRE_PTO`, `UBICACION_PTO`, `LATITUD`, `LONGITUD`, `TIPO_PTO1`, `ID_GESTOR1`, `HORARIO_PTO`, `ESTADO`) VALUES
(120, 'Punto Limpio', 'Las Torcazas / Aguas Calientes (CESFAM Nuevo Horizonte - Villa Los Profesores ). ', -37.4673, -72.3831, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(122, 'Punto Limpio', 'Avda. Costanera Quilque Norte.', -37.4618, -72.3368, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(123, 'Punto Limpio', 'Avda. Nieves Vasquez', -37.4503, -72.333, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(124, 'Punto Limpio', 'Condell, CESFAM 2 de Septiembre', -37.4773, -72.3658, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(125, 'Punto Limpio', 'Misioneros, Altos del Retiro, sector Galilea', -37.4797, -72.3767, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(126, 'Punto Limpio', 'JosÃ© Santos Ossa, Jardines de la Republica', -37.4821, -72.3433, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(127, 'Punto Limpio', 'Rio Cali / Avda. Ferrocarril (SECOF Villa Los Rios)', -37.4907, -72.3283, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(128, 'Punto Limpio', 'Baquedano / Agustinos, Sede JJVV Villa Galilea', -37.4761, -72.3753, 'PuntoLimpio', 31, 'Horario continuado', 'Habilitado'),
(129, 'Campana', 'La Cumbre / Nueva Esperanza (Villa Filadelfia)', -37.4973, -72.3321, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(130, 'Campana', 'Avda. Los Carrera / Avda. Francisco Encina (Salida supermercado UNIMARC)', -37.4878, -72.3429, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(131, 'Campana', 'Avda. Gabriela Mistral / Cordillera (Villa CataluÃ±a) ', -37.4737, -72.3321, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(132, 'Campana', 'Rio Cali / Avda. Ferrocarril (SECOF Villa Los Rios)', -37.4907, -72.3283, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(133, 'Campana', 'Las Quintas / O Higgins', -37.4837, -72.3517, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(134, 'Campana', 'Avda. Ferrocarril / Pedro Jose Benavente', -37.4896, -72.3398, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(135, 'Campana', 'Rio Cali / Buenos Aires (Plaza frente a JJVV Villa Pto. Alegre))', -37.4871, -72.3294, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(136, 'Campana', 'Avda. Lauquen / Avda. Oriente (Plaza Villa Parque Lauquen))', -37.4939, -72.3273, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(137, 'Campana', 'Montreal / Ottawa (JJVV Villa Montreal)', -37.4808, -72.3328, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(138, 'Campana', 'Balbino Sanhueza / Teniente Merino (Block Depto. Rodrigo Quiroga - Sector Paillihue)', -37.4845, -72.3432, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(139, 'Campana', 'Maria Auxiliadora / Jose Santos Ossa (Parque Nativo - Punto Limpio)', -37.4821, -72.3433, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(140, 'Campana', 'Avda. Marconi / Avda. Almirante Latorre (supermercado UNIMARC)', -37.459, -72.3411, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(141, 'Campana', 'Avda. sor Vicenta / Avda. Oriente (Terminal de buses)', -37.4524, -72.3395, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(142, 'Campana', 'Balmaceda / Volcan Lonquimay (Area Verde Hogar Universitario)', -37.4664, -72.3431, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(143, 'Campana', 'Avda. Alemania / Avda. Marconi (Memorial Martires de Antuco)', -37.4688, -72.3376, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(144, 'Campana', 'Avda. Nieves Vasquez / Luxemburgo (Multicancha Villa Parque Sor Vicenta 1 - costado Punto Limpio)', -37.4504, -72.333, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(145, 'Campana', 'Delfos / Macedonia (Plaza - Villa Grecia)', -37.4531, -72.3323, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(146, 'Campana', 'Psje. Guayali / Costanera Quilque Norte (Plaza Villa Guayali)', -37.4641, -72.343, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(147, 'Campana', 'Avda. Costanera Quilque Norte / Nahuelbuta (Parques Nacionales - Punto Limpio)', -37.4618, -72.3368, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(148, 'Campana', 'Almirante La Torre / Avda.  Oriente', -37.457, -72.3348, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(149, 'Campana', 'Almirante La Torre / Davinci', -37.4555, -72.3316, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(150, 'Campana', 'Calle Morse, CESFAM Nororiente', -37.4572, -72.3431, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(151, 'Campana', 'Costanera Quilque Norte / Avda. La Trinidad', -37.4584, -72.3302, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(152, 'Campana', 'Manuel Rioseco / Laguna Verde', -37.443, -72.3393, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(153, 'Campana', 'Esmeralda / Puntilla de Huara', -37.4546, -72.3455, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(154, 'Campana', 'Avda. Ricardo VicuÃ±a / Catirai (Frente a Universidad de Concepcion)', -37.4731, -72.3455, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(155, 'Campana', 'Lynch / O Higgins (Plaza Pinto, sector edificios)', -37.4768, -72.3515, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(156, 'Campana', 'Baquedano / San Martin (Frente edificios - Plaza Pinto)', -37.4757, -72.3528, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(157, 'Campana', 'Ercilla / Orompello', -37.4609, -72.3552, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(158, 'Campana', 'Ercilla entre CaupolicÃ¡n y Ricardo VicuÃ±a (estacionamiento supermercado Jumbo)', -37.4717, -72.3557, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(159, 'Campana', 'Avda. Alemania / Marcos Latapia', -37.4697, -72.3448, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(160, 'Campana', 'Colo-Colo / Valdivia (Pub Deluxe)', -37.4692, -72.3531, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(161, 'Campana', 'Avda. VicuÃ±a Mackenna / Blanco Encalada (Salida EASY)', -37.4811, -72.3584, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(162, 'Campana', 'Avda. Ricardo VicuÃ±a / Alcazar', -37.4723, -72.3621, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(163, 'Campana', 'Condell / Victor Domingo Silva (CESFAM Pob. 2 de Septiembre - Punto Limpio)', -37.4773, -72.3654, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(164, 'Campana', 'Baquedano / Agustinos (Sede JJVV #33 Villa Galilea - Punto Limpio)', -37.4761, -72.3753, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(165, 'Campana', 'Sacramento / Los Misioneros (Altos del Retiro, Sector Galilea - Punto Limpio)', -37.4797, -72.3768, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(166, 'Campana', 'Avda. Padre Hurtado / Baquedano', -37.4754, -72.3701, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(167, 'Campana', 'Valle del Monasterio / Fernando de Aragon', -37.4794, -72.3787, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(168, 'Campana', 'Valle del Monasterio / Ampurias', -37.4824, -72.3779, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(169, 'Campana', 'Temuco / Colo-Colo', -37.4715, -72.3727, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(170, 'Campana', 'Saltos del Velo de la Novia / Saltos del Huilo Huilo, Villa el Retiro Sur', -37.4836, -72.3706, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(171, 'Campana', 'Las Azaleas  / Los Claveles (Salida Supermercado Acuenta)', -37.4671, -72.3669, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(172, 'Campana', 'Avda. Los Angeles / Bombero Rioseco (CESFAM Norte)', -37.4632, -72.362, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(173, 'Campana', 'Las Torcazas / Aguas Calientes (CESFAM Nuevo Horizonte â€“ Villa Los Profesores â€“ Punto Limpio).', -37.4672, -72.383, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(174, 'Campana', 'Bombero Rioseco / Cerro Manquehue (Villa Santiago)', -37.4633, -72.3705, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(175, 'Campana', 'Lago Neltume / Padre Hurtado (Villa Padre Hurtado)', -37.4618, -72.3729, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(176, 'Campana', 'Bombero Rioseco / Fortunato de la Maza (Plaza Araucania) ', -37.4628, -72.3585, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(177, 'Campana', 'Orompello / Raulies', -37.4607, -72.3686, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(178, 'Campana', 'Orompello, Acceso a condominios Andino y Cordillera', -37.4508, -72.3875, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(179, 'Campana', 'Alamedas de San Francisco (km. 8 - Camino Cerro Colorado)', -37.4137, -72.2469, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(180, 'Campana', 'Saltos del Laja (Area restaurantes y hosterias)', -37.2193, -72.3831, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(181, 'Campana', 'Avda. BioBio / Caupolican, San Carlos de Puren ', -37.5968, -72.2778, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(182, 'Campana', 'Lautaro / Millarrahue (Plaza), San Carlos de Puren', -37.5956, -72.2732, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(183, 'Campana', 'Country Santa Catalina, Camino a Santa Barbara', -37.5235, -72.2376, 'Campana', 30, 'Horario continuado', 'Habilitado'),
(184, 'Contenedor Rejilla', 'CESFAM Sur, Paillihue', -37.4852, -72.3392, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(185, 'Contenedor Rejilla', 'Avda. Gabriela Mistral / Cordillera (Villa CataluÃ±a) ', -37.4737, -72.332, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(186, 'Contenedor Rejilla', 'Gabriela Mistral / Investigaciones de Chile', -37.4768, -72.347, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(187, 'Contenedor Rejilla', 'Las Quintas / Comandante Oscar Tapia', -37.4839, -72.3495, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(188, 'Contenedor Rejilla', 'Gabriela Mistral, altura Petrobras', -37.4736, -72.3293, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(190, 'Contenedor Rejilla', 'Rio Cali / Buenos Aires (Plaza frente a JJVV Villa Pto. Alegre))', -37.4871, -72.3292, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(191, 'Contenedor Rejilla', 'Avda. Lauquen / Avda. Oriente (Plaza Villa Parque Lauquen))', -37.4939, -72.3274, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(192, 'Contenedor Rejilla', 'Montreal / Ottawa (JJVV Villa Montreal)', -37.4808, -72.3329, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(193, 'Contenedor Rejilla', 'Polideportivo', -37.4739, -72.3363, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(194, 'Contenedor Rejilla', 'San Pedro / San Agustin, Villa Todos Los Santos (receptaculo de cemento en area verde por San Pedro)', -37.4869, -72.354, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(195, 'Contenedor Rejilla', 'Pje. Cumbre / Pje. Sur, Villa Parque Lauquen', -37.494, -72.3323, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(196, 'Contenedor Rejilla', 'Rio Cholguague / Rio Huaqui, Poblacion 21 de Mayo', -37.4882, -72.3458, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(197, 'Contenedor Rejilla', 'Balbino Sanhueza / Teniente Merino (Deptos. Rodrigo de Quiroga)', -37.4844, -72.343, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(198, 'Contenedor Rejilla', 'Los Apaches / Los Mitimaes, Villa Las Americas (adosado a cierre perimetral de sede JJVV)', -37.4871, -72.3349, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(199, 'Contenedor Rejilla', 'Avda. Marconi / Avda. Almirante Latorre (supermercado UNIMARC)', -37.4589, -72.3412, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(200, 'Contenedor Rejilla', 'Avda. Marconi / Avda. Almirante Latorre (supermercado UNIMARC)', -37.4588, -72.341, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(201, 'Contenedor Rejilla', 'Luxemburgo / Avda. Oriente (terminal de buses)', -37.4526, -72.3393, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(202, 'Contenedor Rejilla', 'Luxemburgo / Avda. Oriente (terminal de buses)', -37.4524, -72.3395, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(203, 'Contenedor Rejilla', 'Balmaceda / Volcan Lonquimay (Area Verde Hogar Universitario)', -37.4663, -72.3431, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(204, 'Contenedor Rejilla', 'Avda. Alemania / Avda. Marconi (Memorial Martires de Antuco)', -37.4691, -72.3374, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(205, 'Contenedor Rejilla', 'Baldometo Duble / Ricardo Castro, Pedro Lagos ', -37.4661, -72.3411, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(206, 'Contenedor Rejilla', 'Delfos / Macedonia (Plaza - Villa Grecia)', -37.4534, -72.3322, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(208, 'Contenedor Rejilla', 'Avda. Almirante La Torre / Da Vinci', -37.4555, -72.3316, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(209, 'Contenedor Rejilla', 'Roma / Florencia, Villa Italia', -37.4566, -72.3411, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(210, 'Contenedor Rejilla', 'Avda. Costanera Quilque Norte / Pje. Montalban', -37.4612, -72.3355, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(211, 'Contenedor Rejilla', 'Samuel Morse / Bell, Pob. Endesa, CESFAM Nororiente', -37.4573, -72.3433, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(212, 'Contenedor Rejilla', 'Guayali / Cost. Quilque Norte (Plaza Pob. Guayali)', -37.464, -72.3428, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(213, 'Contenedor Rejilla', 'Avda. Almirante La Torre / Avda. Oriente', -37.457, -72.3348, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(214, 'Contenedor Rejilla', 'Laguna Verde / Dr. Rioseco (Colegio San Gabriel)', -37.4429, -72.3393, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(215, 'Contenedor Rejilla', 'Almirante La Torre / Chonos-Pehuenches, Plaza Santa Maria', -37.4598, -72.3446, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(216, 'Contenedor Rejilla', 'Avda. La Trinidad / Costanera Quilque Norte', -37.4583, -72.33, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(217, 'Contenedor Rejilla', 'Nestor del Rio / Chorrillos', -37.4573, -72.3498, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(218, 'Contenedor Rejilla', 'Jardines de Luxemburgo / Corcolen, Villa Jardines de Luxemburgo', -37.4518, -72.3349, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(219, 'Contenedor Rejilla', 'Calle Estados Unidos / Rusia, Pob .Galvarino (Locales comerciales)', -37.4626, -72.3448, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(220, 'Contenedor Rejilla', 'Calle Local / Avda. Tres Vientos, Villa Tres Vientos, (El Avellano)', -37.4428, -72.3296, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(221, 'Contenedor Rejilla', 'Calle Altos del Refugio / Altos del Refugio 2 (Parque de juegos, salida Cond.)', -37.4433, -72.3344, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(222, 'Contenedor Rejilla', 'Nevados de Chillan / Panivamida, Villa Nevados de Chillan (parque)', -37.4456, -72.3338, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(223, 'Contenedor Rejilla', 'Puntilla de Huara / Marconi, acceso Villa Parque Norte', -37.4545, -72.3454, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(224, 'Contenedor Rejilla', 'Costanera Quilque Norte / Arrayan (acceso condominio el Roble)', -37.4634, -72.3413, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(225, 'Contenedor Rejilla', 'Costanera Quilque Sur / Calle Estero, Villa Mirador de Los Andes (poste de alumbrado por Cost. Quilque Sur)', -37.463, -72.339, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(226, 'Contenedor Rejilla', 'Volcan Calbuco #355, Pob. Domingo Contreras Gomez, Escuela Republica de Grecia', -37.4672, -72.3454, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(227, 'Contenedor Rejilla', 'Orompello / Pje. Los Mapuches, Villa Obispo', -37.4607, -72.3441, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(228, 'Contenedor Rejilla', 'Condominio El Avellano (contenedor tipo caseta)', -37.4485, -72.3382, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(229, 'Contenedor Rejilla', 'Almirante La Torre / Almagro', -37.4589, -72.348, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(230, 'Contenedor Rejilla', 'Avda. Ricardo VicuÃ±a / Catirai (Frente a Universidad de Concepcion)', -37.4726, -72.3454, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(231, 'Contenedor Rejilla', 'Linch / O Higgins (Plaza Pinto, sector edificios)', -37.4768, -72.3516, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(232, 'Contenedor Rejilla', 'Baquedano / San Martin (Frente edificios - Plaza Pinto)', -37.4758, -72.3528, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(233, 'Contenedor Rejilla', 'Acceso Hospital Dr. Victor Rios Ruiz', -37.4732, -72.3462, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(234, 'Contenedor Rejilla', 'Medoza / Paseo Ronald Ram (Instituto AIEP, contenedor tipo caseta)', -37.4688, -72.3539, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(235, 'Contenedor Rejilla', 'Pasaje San Severino / Avda. Padre Hurtado', -37.4757, -72.3693, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(236, 'Contenedor Rejilla', 'Avda. Ricardo VicuÃ±a / Alcazar', -37.4723, -72.3621, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(237, 'Contenedor Rejilla', 'Valle del Monasterio / Ampurias', -37.4824, -72.3779, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(238, 'Contenedor Rejilla', 'Valle del Monasterio / Fernando de Aragon', -37.4794, -72.3786, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(239, 'Contenedor Rejilla', 'Las Azaleaas / Pje. Eucaliptus, Pob. Kennedy', -37.4712, -72.3657, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(240, 'Contenedor Rejilla', 'Av. Los Angeles / Los Robles, Estadio Municipal', -37.471, -72.361, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(241, 'Contenedor Rejilla', 'Lynch / Pedro Luna, Pob. 2 de Septiembre', -37.4767, -72.3653, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(242, 'Contenedor Rejilla', 'Area verde, calle Daniel De La Vega, Pob Real Victoria', -37.4727, -72.3645, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(243, 'Contenedor Rejilla', 'Mendoza / Chorrillo, plazoleta ', -37.4574, -72.3535, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(244, 'Contenedor Rejilla', 'Acceso condominia Curamonte (frene a cementerio Parque del Sur)', -37.4478, -72.3927, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(245, 'Contenedor Rejilla', 'Las Azaleas /Cost. Quilque Norte (Supermercado Acuenta)', -37.4671, -72.3671, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(246, 'Contenedor Rejilla', 'Ruiz Aldea / Bombero Rioseco (CESFAM Norte)', -37.4631, -72.3625, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(247, 'Contenedor Rejilla', 'Condominio Andino (acceso por Orompello, Cont. tipo caseta)', -37.45, -72.389, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(248, 'Contenedor Rejilla', 'Bombero Rioseco / Cerro Manquehue (Villa Santiago)', -37.4633, -72.3705, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(249, 'Contenedor Rejilla', 'Bombero Rioseco / Fortunato de la Maza (Plaza Araucania) ', -37.4628, -72.3583, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(250, 'Contenedor Rejilla', 'Prat / Colo-Colo (Salida INCUBA BioBio)', -37.469, -72.3572, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(251, 'Contenedor Rejilla', 'Los Raulies / Orompello (Supermercado La Amistad) Villa Sante Fe', -37.4605, -72.3689, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(252, 'Contenedor Rejilla', 'Los Sauces / Los Ulmos. Villa Santa Fe', -37.4581, -72.3695, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(253, 'Contenedor Rejilla', 'Condominio Cordillera (acceso por Orompello, contenedor tipo caseta)', -37.4501, -72.3891, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(254, 'Contenedor Rejilla', 'Av. Poniente / Costanera Quilque Sur (estacionamiento frente a cancha Futsal Parque Estero Quilque)', -37.4686, -72.3756, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(255, 'Contenedor Rejilla', 'Colo-Colo / Temuco, Ciudades de Chile', -37.4715, -72.3727, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(256, 'Contenedor Rejilla', 'Calle Temuco / Costanera Quilque Sur (Estacionamiento Parque Estero Quilque, frente a anfiteatro)', -37.4686, -72.3733, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(257, 'Contenedor Rejilla', 'Bombero Rioseco / Pje. El Pino, Villa Los Profesores (salida jardin infantil \"Pasito Seguro\")', -37.4653, -72.3808, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(258, 'Contenedor Rejilla', 'Chile Lindo / Pje. Tamure, Villa Genesis', -37.4646, -72.4035, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(259, 'Contenedor Rejilla', 'Chile Lindo, altura copa de agua, Villa Genesis', -37.465, -72.4013, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(260, 'Contenedor Rejilla', 'Chile Lindo, acceso Villa Genesis', -37.4668, -72.3952, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(261, 'Contenedor Rejilla', 'Lago Neltume / Padre Hurtado (Pob. Lagos de Chile)', -37.4619, -72.3728, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(262, 'Contenedor Rejilla', 'Nahuelbuta / Cost. Quilque Norte, Parque Estero Quilque', -37.4699, -72.3804, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(263, 'Contenedor Rejilla', 'Las Gardenias / Las Margaritas, Pob. O Higgins', -37.4659, -72.3641, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(264, 'Contenedor Rejilla', 'Las Azucenas / Las Portilacas, Pob. O Higgins', -37.4626, -72.3651, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(265, 'Contenedor Rejilla', 'Isla Nueva / Galvarino, Blocks departamentos', -37.4659, -72.3699, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(266, 'Contenedor Rejilla', 'Costanera Quilque Norte / Ruiz Aldea, Complejo deportivo Orompello', -37.4672, -72.3623, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(267, 'Contenedor Rejilla', 'Camino a Nacimiento (km 5), Country Santa Eliana', -37.4831, -72.4241, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(268, 'Contenedor Rejilla', 'Av. BioBio / Caupolican, San Carlos de Puren', -37.5968, -72.2778, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(269, 'Contenedor Rejilla', 'San Carlos / Millarehue (Escuela Etilhue), San Carlos de Puren', -37.5943, -72.2761, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(270, 'Contenedor Rejilla', 'Lautaro / Millarapue (Plaza), San Carlos de Puren', -37.5957, -72.2732, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(271, 'Contenedor Rejilla', 'Av. BioBio / San Carlos (Reten de Carabineros), San Carlos de Puren', -37.5942, -72.2777, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(272, 'Contenedor Rejilla', 'Ines de Suarez / Pedro de Valdivia, San Carlos de Puren', -37.5979, -72.2734, 'Contenedor', 29, 'Horario continuado', 'Habilitado'),
(273, 'Contenedor Rejilla', 'O Higgins / Noemi Escobar, Sector Santa Fe (frente a CESFAM)', -37.4664, -72.582, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(274, 'Contenedor Rejilla', 'Av. O Higgins, salida Jardin Kimeltu, sector Santa Fe ', -37.4665, -72.58, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(275, 'Contenedor Rejilla', 'Av. O Higgins / Miguel Sagardia (Ã¡rea verde paradero), Sector Millantu', -37.3974, -72.6199, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(276, 'Contenedor Rejilla', 'Acceso Posta Virquenco, Calle Carlos Bordeau', -37.4675, -72.5006, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(277, 'Contenedor Rejilla', 'Carlos Bordeau /  Monica Bordeau, Virquenco (al costado derecho del paradero)', -37.4575, -72.4989, 'Contenedor', 27, 'Horario continuado', 'Habilitado'),
(279, 'Contenedor Rejilla ', 'Avda. Marconi / Avda. Almirante Latorre (supermercado UNIMARC)', -37.4591, -72.3413, 'Contenedor', 27, '24 hrs ', 'Habilitado'),
(281, 'Campana', 'Avda. Marconi / Avda. Almirante Latorre (Supermercado UIMARC)', -37.4588, -72.3409, 'Campana', 30, '24 hrs', 'Habilitado'),
(282, 'Campana', 'Avda. Alemania / Avda. Marconi (Memorial MÃ¡rtires de Antuco)', -37.4692, -72.3379, 'Campana', 30, '24 hrs', 'Habilitado'),
(283, 'Campana', 'Avda. Nieves Vasquez / Luxemburgo (Multicancha Villa Parque Sor Vicenta 1 - costado Punto Limpio', -37.4505, -72.3325, 'Campana', 30, '24 hrs', 'Habilitado'),
(284, 'Campana', 'Avda. Costanera Quilque Norte / Nahuelbuta (Parques Nacionales - Punto Limpio)', -37.4701, -72.3808, 'Campana', 30, '24 hrs', 'Habilitado'),
(285, 'Campana', 'Avda. VicuÃ±a Mackenna / Blanco Encalada (Salida EASY)', -37.4811, -72.359, 'Campana', 30, '24 hrs', 'Habilitado'),
(286, 'Campana', 'Ines de Suarez / Pedro de Valdivia, San Carlos de Puren', -37.5979, -72.2736, 'Campana', 30, '24 hrs', 'Habilitado'),
(287, 'Contenedor Rejilla', 'Avda. Alemania / Avda. Marconi (Memorial Martires de Antuco)', -37.469, -72.3371, 'Contenedor', 29, '24 hrs', 'Habilitado'),
(289, 'Contenedor Rejilla de prueba', 'Avda. VicuÃ±a Mackena esquina Camilo Henriquez', -37.4802, -72.3613, 'Contenedor', 29, '24 hrs ', 'Habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes`
--

CREATE TABLE `reportes` (
  `ID_REPORTE` int(11) NOT NULL,
  `ID_PUNTO` int(8) NOT NULL,
  `DESCRIPCION` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contiene los reportes emitidos sobre el funcionamiento de los puntos verdes y puntos limpios';

--
-- Volcado de datos para la tabla `reportes`
--

INSERT INTO `reportes` (`ID_REPORTE`, `ID_PUNTO`, `DESCRIPCION`) VALUES
(22, 160, 'No han sido retirados los residuos de la instalaciÃ³n hace mÃ¡s de un mes. Ya no es posible seguir utilizando el punto verde.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

CREATE TABLE `solicitudes` (
  `ID_SOLICITUD` int(8) NOT NULL,
  `NOMBRE_PTO` varchar(200) NOT NULL,
  `UBICACION_PTO` varchar(200) NOT NULL,
  `LATITUD` float NOT NULL,
  `LONGITUD` float NOT NULL,
  `TIPO_PTO1` varchar(15) NOT NULL,
  `ID_GESTOR1` int(11) NOT NULL,
  `HORARIO_PTO` varchar(300) NOT NULL,
  `ESTADO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contiene las solicitudes de registro de puntos verdes y puntos limpios emitidas por los gestores de residuos';

--
-- Volcado de datos para la tabla `solicitudes`
--

INSERT INTO `solicitudes` (`ID_SOLICITUD`, `NOMBRE_PTO`, `UBICACION_PTO`, `LATITUD`, `LONGITUD`, `TIPO_PTO1`, `ID_GESTOR1`, `HORARIO_PTO`, `ESTADO`) VALUES
(1, 'Contenedor Rejilla ', 'Avda. Marconi / Avda. Almirante Latorre (supermercado UNIMARC)', -37.4591, -72.3413, 'Contenedor', 27, '24 hrs ', 'APROBADO'),
(4, 'Campana', 'Pasaje San Severino / Avda. Padre Hurtado', -37.4756, -72.3693, 'Campana', 29, 'Horario continuado', 'PENDIENTE'),
(5, 'Contenedor Rejilla de prueba', 'Avda. VicuÃ±a Mackena esquina Camilo Henriquez', -37.4802, -72.3613, 'Contenedor', 29, '24 hrs ', 'APROBADO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`ID_ADMIN`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`TIPO_PUNTO`);

--
-- Indices de la tabla `gestores`
--
ALTER TABLE `gestores`
  ADD PRIMARY KEY (`ID_GESTOR`);

--
-- Indices de la tabla `punto_verde`
--
ALTER TABLE `punto_verde`
  ADD PRIMARY KEY (`ID_PUNTO`),
  ADD KEY `TIPO_PTO1` (`TIPO_PTO1`),
  ADD KEY `ID_ADMIN1` (`ID_GESTOR1`);

--
-- Indices de la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`ID_REPORTE`),
  ADD KEY `ID_PUNTO` (`ID_PUNTO`) USING BTREE;

--
-- Indices de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD PRIMARY KEY (`ID_SOLICITUD`),
  ADD KEY `TIPO_PTO1` (`TIPO_PTO1`),
  ADD KEY `ID_ADMIN1` (`ID_GESTOR1`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `ID_ADMIN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gestores`
--
ALTER TABLE `gestores`
  MODIFY `ID_GESTOR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `punto_verde`
--
ALTER TABLE `punto_verde`
  MODIFY `ID_PUNTO` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT de la tabla `reportes`
--
ALTER TABLE `reportes`
  MODIFY `ID_REPORTE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  MODIFY `ID_SOLICITUD` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `punto_verde`
--
ALTER TABLE `punto_verde`
  ADD CONSTRAINT `punto_verde_ibfk_1` FOREIGN KEY (`TIPO_PTO1`) REFERENCES `categorias` (`TIPO_PUNTO`) ON UPDATE CASCADE,
  ADD CONSTRAINT `punto_verde_ibfk_2` FOREIGN KEY (`ID_GESTOR1`) REFERENCES `gestores` (`ID_GESTOR`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD CONSTRAINT `reportes_ibfk_1` FOREIGN KEY (`ID_PUNTO`) REFERENCES `punto_verde` (`ID_PUNTO`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD CONSTRAINT `solicitudes_ibfk_1` FOREIGN KEY (`ID_GESTOR1`) REFERENCES `gestores` (`ID_GESTOR`) ON UPDATE CASCADE,
  ADD CONSTRAINT `solicitudes_ibfk_2` FOREIGN KEY (`TIPO_PTO1`) REFERENCES `categorias` (`TIPO_PUNTO`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
