{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.341,
          -37.4588
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Marconi \\/ Avda. Almirante Latorre (supermercado UNIMARC)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "200"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3395,
          -37.4524
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Luxemburgo \\/ Avda. Oriente (terminal de buses)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "202"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3322,
          -37.4534
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Delfos \\/ Macedonia (Plaza - Villa Grecia)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "206"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3411,
          -37.4566
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Roma \\/ Florencia, Villa Italia",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "209"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3355,
          -37.4612
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Costanera Quilque Norte \\/ Pje. Montalban",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "210"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3433,
          -37.4573
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Samuel Morse \\/ Bell, Pob. Endesa, CESFAM Nororiente",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "211"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3348,
          -37.457
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Almirante La Torre \\/ Avda. Oriente",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "213"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3528,
          -37.4758
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Baquedano \\/ San Martin (Frente edificios - Plaza Pinto)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "232"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3462,
          -37.4732
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Acceso Hospital Dr. Victor Rios Ruiz",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "233"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3539,
          -37.4688
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Medoza \\/ Paseo Ronald Ram (Instituto AIEP, contenedor tipo caseta)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "234"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3693,
          -37.4757
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Pasaje San Severino \\/ Avda. Padre Hurtado",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "235"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3621,
          -37.4723
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Ricardo Vicuña \\/ Alcazar",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "236"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3779,
          -37.4824
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Valle del Monasterio \\/ Ampurias",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "237"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3786,
          -37.4794
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Valle del Monasterio \\/ Fernando de Aragon",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "238"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3657,
          -37.4712
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Las Azaleaas \\/ Pje. Eucaliptus, Pob. Kennedy",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "239"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.361,
          -37.471
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. Los Angeles \\/ Los Robles, Estadio Municipal",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "240"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3653,
          -37.4767
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Lynch \\/ Pedro Luna, Pob. 2 de Septiembre",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "241"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3645,
          -37.4727
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Area verde, calle Daniel De La Vega, Pob Real Victoria",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "242"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3535,
          -37.4574
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Mendoza \\/ Chorrillo, plazoleta ",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "243"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3927,
          -37.4478
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Acceso condominia Curamonte (frene a cementerio Parque del Sur)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "244"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3671,
          -37.4671
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Las Azaleas \\/Cost. Quilque Norte (Supermercado Acuenta)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "245"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3625,
          -37.4631
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Ruiz Aldea \\/ Bombero Rioseco (CESFAM Norte)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "246"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.389,
          -37.45
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Condominio Andino (acceso por Orompello, Cont. tipo caseta)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "247"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3705,
          -37.4633
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Bombero Rioseco \\/ Cerro Manquehue (Villa Santiago)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "248"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3583,
          -37.4628
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Bombero Rioseco \\/ Fortunato de la Maza (Plaza Araucania) ",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "249"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3572,
          -37.469
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Prat \\/ Colo-Colo (Salida INCUBA BioBio)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "250"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3689,
          -37.4605
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Los Raulies \\/ Orompello (Supermercado La Amistad) Villa Sante Fe",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "251"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3695,
          -37.4581
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Los Sauces \\/ Los Ulmos. Villa Santa Fe",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "252"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3891,
          -37.4501
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Condominio Cordillera (acceso por Orompello, contenedor tipo caseta)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "253"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3756,
          -37.4686
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. Poniente \\/ Costanera Quilque Sur (estacionamiento frente a cancha Futsal Parque Estero Quilque)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "254"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3727,
          -37.4715
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Colo-Colo \\/ Temuco, Ciudades de Chile",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "255"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3733,
          -37.4686
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Calle Temuco \\/ Costanera Quilque Sur (Estacionamiento Parque Estero Quilque, frente a anfiteatro)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "256"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3808,
          -37.4653
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Bombero Rioseco \\/ Pje. El Pino, Villa Los Profesores (salida jardin infantil \"Pasito Seguro\")",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "257"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.4035,
          -37.4646
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Chile Lindo \\/ Pje. Tamure, Villa Genesis",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "258"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.4013,
          -37.465
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Chile Lindo, altura copa de agua, Villa Genesis",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "259"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3952,
          -37.4668
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Chile Lindo, acceso Villa Genesis",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "260"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3728,
          -37.4619
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Lago Neltume \\/ Padre Hurtado (Pob. Lagos de Chile)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "261"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3804,
          -37.4699
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Nahuelbuta \\/ Cost. Quilque Norte, Parque Estero Quilque",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "262"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3641,
          -37.4659
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Las Gardenias \\/ Las Margaritas, Pob. O Higgins",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "263"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3651,
          -37.4626
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Las Azucenas \\/ Las Portilacas, Pob. O Higgins",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "264"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3699,
          -37.4659
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Isla Nueva \\/ Galvarino, Blocks departamentos",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "265"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3623,
          -37.4672
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Costanera Quilque Norte \\/ Ruiz Aldea, Complejo deportivo Orompello",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "266"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.4241,
          -37.4831
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Camino a Nacimiento (km 5), Country Santa Eliana",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "267"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.582,
          -37.4664
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "O Higgins \\/ Noemi Escobar, Sector Santa Fe (frente a CESFAM)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "273"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.58,
          -37.4665
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. O Higgins, salida Jardin Kimeltu, sector Santa Fe ",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "274"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.6199,
          -37.3974
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. O Higgins \\/ Miguel Sagardia (área verde paradero), Sector Millantu",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "275"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.5006,
          -37.4675
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Acceso Posta Virquenco, Calle Carlos Bordeau",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "276"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.4989,
          -37.4575
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Carlos Bordeau \\/  Monica Bordeau, Virquenco (al costado derecho del paradero)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "277"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3413,
          -37.4591
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla ",
        "Ubicacion": "Avda. Marconi \\/ Avda. Almirante Latorre (supermercado UNIMARC)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "24 hrs ",
        "Contacto": "MyF",
        "Emaildecontacto": "correo_myf@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "279"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3392,
          -37.4852
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "CESFAM Sur, Paillihue",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "184"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.332,
          -37.4737
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Gabriela Mistral \\/ Cordillera (Villa Cataluña) ",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "185"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.347,
          -37.4768
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Gabriela Mistral \\/ Investigaciones de Chile",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "186"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3495,
          -37.4839
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Las Quintas \\/ Comandante Oscar Tapia",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "187"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3293,
          -37.4736
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Gabriela Mistral, altura Petrobras",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "188"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3292,
          -37.4871
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Rio Cali \\/ Buenos Aires (Plaza frente a JJVV Villa Pto. Alegre))",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "190"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3274,
          -37.4939
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Lauquen \\/ Avda. Oriente (Plaza Villa Parque Lauquen))",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "191"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3329,
          -37.4808
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Montreal \\/ Ottawa (JJVV Villa Montreal)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "192"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3363,
          -37.4739
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Polideportivo",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "193"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.354,
          -37.4869
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "San Pedro \\/ San Agustin, Villa Todos Los Santos (receptaculo de cemento en area verde por San Pedro)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "194"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3323,
          -37.494
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Pje. Cumbre \\/ Pje. Sur, Villa Parque Lauquen",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "195"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3458,
          -37.4882
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Rio Cholguague \\/ Rio Huaqui, Poblacion 21 de Mayo",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "196"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.343,
          -37.4844
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Balbino Sanhueza \\/ Teniente Merino (Deptos. Rodrigo de Quiroga)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "197"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3349,
          -37.4871
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Los Apaches \\/ Los Mitimaes, Villa Las Americas (adosado a cierre perimetral de sede JJVV)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "198"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3412,
          -37.4589
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Marconi \\/ Avda. Almirante Latorre (supermercado UNIMARC)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "199"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3393,
          -37.4526
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Luxemburgo \\/ Avda. Oriente (terminal de buses)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "201"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3431,
          -37.4663
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Balmaceda \\/ Volcan Lonquimay (Area Verde Hogar Universitario)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "203"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3374,
          -37.4691
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Alemania \\/ Avda. Marconi (Memorial Martires de Antuco)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "204"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3411,
          -37.4661
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Baldometo Duble \\/ Ricardo Castro, Pedro Lagos ",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "205"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3316,
          -37.4555
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Almirante La Torre \\/ Da Vinci",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "208"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3428,
          -37.464
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Guayali \\/ Cost. Quilque Norte (Plaza Pob. Guayali)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "212"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3393,
          -37.4429
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Laguna Verde \\/ Dr. Rioseco (Colegio San Gabriel)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "214"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3446,
          -37.4598
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Almirante La Torre \\/ Chonos-Pehuenches, Plaza Santa Maria",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "215"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.33,
          -37.4583
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. La Trinidad \\/ Costanera Quilque Norte",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "216"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3498,
          -37.4573
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Nestor del Rio \\/ Chorrillos",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "217"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3349,
          -37.4518
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Jardines de Luxemburgo \\/ Corcolen, Villa Jardines de Luxemburgo",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "218"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3448,
          -37.4626
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Calle Estados Unidos \\/ Rusia, Pob .Galvarino (Locales comerciales)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "219"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3296,
          -37.4428
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Calle Local \\/ Avda. Tres Vientos, Villa Tres Vientos, (El Avellano)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "220"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3344,
          -37.4433
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Calle Altos del Refugio \\/ Altos del Refugio 2 (Parque de juegos, salida Cond.)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "221"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3338,
          -37.4456
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Nevados de Chillan \\/ Panivamida, Villa Nevados de Chillan (parque)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "222"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3454,
          -37.4545
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Puntilla de Huara \\/ Marconi, acceso Villa Parque Norte",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "223"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3413,
          -37.4634
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Costanera Quilque Norte \\/ Arrayan (acceso condominio el Roble)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "224"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.339,
          -37.463
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Costanera Quilque Sur \\/ Calle Estero, Villa Mirador de Los Andes (poste de alumbrado por Cost. Quilque Sur)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "225"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3454,
          -37.4672
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Volcan Calbuco #355, Pob. Domingo Contreras Gomez, Escuela Republica de Grecia",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "226"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3441,
          -37.4607
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Orompello \\/ Pje. Los Mapuches, Villa Obispo",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "227"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3382,
          -37.4485
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Condominio El Avellano (contenedor tipo caseta)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "228"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.348,
          -37.4589
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Almirante La Torre \\/ Almagro",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "229"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3454,
          -37.4726
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Ricardo Vicuña \\/ Catirai (Frente a Universidad de Concepcion)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "230"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3516,
          -37.4768
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Linch \\/ O Higgins (Plaza Pinto, sector edificios)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "231"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2778,
          -37.5968
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. BioBio \\/ Caupolican, San Carlos de Puren",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "268"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2761,
          -37.5943
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "San Carlos \\/ Millarehue (Escuela Etilhue), San Carlos de Puren",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "269"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2732,
          -37.5957
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Lautaro \\/ Millarapue (Plaza), San Carlos de Puren",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "270"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2777,
          -37.5942
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Av. BioBio \\/ San Carlos (Reten de Carabineros), San Carlos de Puren",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "271"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2734,
          -37.5979
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Ines de Suarez \\/ Pedro de Valdivia, San Carlos de Puren",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "Horario continuado",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "272"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3371,
          -37.469
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla",
        "Ubicacion": "Avda. Alemania \\/ Avda. Marconi (Memorial Martires de Antuco)",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "24 hrs",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "287"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3613,
          -37.4802
        ]
      },
      "properties": {
        "Nombre": "Contenedor Rejilla de prueba",
        "Ubicacion": "Avda. Vicuña Mackena esquina Camilo Henriquez",
        "TipodePunto": "Contenedor",
        "Elementosquerecibe": "Botellas y envases de material tipo PET 1.",
        "Horario": "24 hrs ",
        "Contacto": "Greenwalk",
        "Emaildecontacto": "correo_greenwalk@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "289"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3321,
          -37.4973
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "La Cumbre \\/ Nueva Esperanza (Villa Filadelfia)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "129"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3429,
          -37.4878
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Los Carrera \\/ Avda. Francisco Encina (Salida supermercado UNIMARC)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "130"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3321,
          -37.4737
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Gabriela Mistral \\/ Cordillera (Villa Cataluña) ",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "131"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3283,
          -37.4907
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Rio Cali \\/ Avda. Ferrocarril (SECOF Villa Los Rios)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "132"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3517,
          -37.4837
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Las Quintas \\/ O Higgins",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "133"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3398,
          -37.4896
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Ferrocarril \\/ Pedro Jose Benavente",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "134"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3294,
          -37.4871
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Rio Cali \\/ Buenos Aires (Plaza frente a JJVV Villa Pto. Alegre))",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "135"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3273,
          -37.4939
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Lauquen \\/ Avda. Oriente (Plaza Villa Parque Lauquen))",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "136"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3328,
          -37.4808
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Montreal \\/ Ottawa (JJVV Villa Montreal)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "137"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3432,
          -37.4845
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Balbino Sanhueza \\/ Teniente Merino (Block Depto. Rodrigo Quiroga - Sector Paillihue)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "138"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3433,
          -37.4821
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Maria Auxiliadora \\/ Jose Santos Ossa (Parque Nativo - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "139"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3411,
          -37.459
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Marconi \\/ Avda. Almirante Latorre (supermercado UNIMARC)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "140"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3395,
          -37.4524
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. sor Vicenta \\/ Avda. Oriente (Terminal de buses)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "141"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3431,
          -37.4664
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Balmaceda \\/ Volcan Lonquimay (Area Verde Hogar Universitario)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "142"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3376,
          -37.4688
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Alemania \\/ Avda. Marconi (Memorial Martires de Antuco)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "143"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.333,
          -37.4504
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Nieves Vasquez \\/ Luxemburgo (Multicancha Villa Parque Sor Vicenta 1 - costado Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "144"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3323,
          -37.4531
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Delfos \\/ Macedonia (Plaza - Villa Grecia)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "145"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.343,
          -37.4641
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Psje. Guayali \\/ Costanera Quilque Norte (Plaza Villa Guayali)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "146"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3368,
          -37.4618
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Costanera Quilque Norte \\/ Nahuelbuta (Parques Nacionales - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "147"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3348,
          -37.457
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Almirante La Torre \\/ Avda.  Oriente",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "148"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3316,
          -37.4555
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Almirante La Torre \\/ Davinci",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "149"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3431,
          -37.4572
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Calle Morse, CESFAM Nororiente",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "150"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3302,
          -37.4584
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Costanera Quilque Norte \\/ Avda. La Trinidad",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "151"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3393,
          -37.443
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Manuel Rioseco \\/ Laguna Verde",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "152"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3455,
          -37.4546
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Esmeralda \\/ Puntilla de Huara",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "153"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3455,
          -37.4731
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Ricardo Vicuña \\/ Catirai (Frente a Universidad de Concepcion)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "154"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3515,
          -37.4768
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Lynch \\/ O Higgins (Plaza Pinto, sector edificios)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "155"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3528,
          -37.4757
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Baquedano \\/ San Martin (Frente edificios - Plaza Pinto)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "156"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3552,
          -37.4609
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Ercilla \\/ Orompello",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "157"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3557,
          -37.4717
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Ercilla entre Caupolicán y Ricardo Vicuña (estacionamiento supermercado Jumbo)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "158"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3448,
          -37.4697
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Alemania \\/ Marcos Latapia",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "159"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3531,
          -37.4692
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Colo-Colo \\/ Valdivia (Pub Deluxe)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "160"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3584,
          -37.4811
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Vicuña Mackenna \\/ Blanco Encalada (Salida EASY)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "161"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3621,
          -37.4723
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Ricardo Vicuña \\/ Alcazar",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "162"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3654,
          -37.4773
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Condell \\/ Victor Domingo Silva (CESFAM Pob. 2 de Septiembre - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "163"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3753,
          -37.4761
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Baquedano \\/ Agustinos (Sede JJVV #33 Villa Galilea - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "164"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3768,
          -37.4797
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Sacramento \\/ Los Misioneros (Altos del Retiro, Sector Galilea - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "165"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3701,
          -37.4754
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Padre Hurtado \\/ Baquedano",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "166"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3787,
          -37.4794
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Valle del Monasterio \\/ Fernando de Aragon",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "167"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3779,
          -37.4824
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Valle del Monasterio \\/ Ampurias",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "168"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3727,
          -37.4715
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Temuco \\/ Colo-Colo",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "169"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3706,
          -37.4836
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Saltos del Velo de la Novia \\/ Saltos del Huilo Huilo, Villa el Retiro Sur",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "170"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3669,
          -37.4671
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Las Azaleas  \\/ Los Claveles (Salida Supermercado Acuenta)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "171"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.362,
          -37.4632
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Los Angeles \\/ Bombero Rioseco (CESFAM Norte)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "172"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.383,
          -37.4672
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Las Torcazas \\/ Aguas Calientes (CESFAM Nuevo Horizonte – Villa Los Profesores – Punto Limpio).",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "173"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3705,
          -37.4633
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Bombero Rioseco \\/ Cerro Manquehue (Villa Santiago)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "174"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3729,
          -37.4618
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Lago Neltume \\/ Padre Hurtado (Villa Padre Hurtado)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "175"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3585,
          -37.4628
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Bombero Rioseco \\/ Fortunato de la Maza (Plaza Araucania) ",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "176"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3686,
          -37.4607
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Orompello \\/ Raulies",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "177"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3875,
          -37.4508
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Orompello, Acceso a condominios Andino y Cordillera",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "178"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2469,
          -37.4137
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Alamedas de San Francisco (km. 8 - Camino Cerro Colorado)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "179"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3831,
          -37.2193
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Saltos del Laja (Area restaurantes y hosterias)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "180"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2778,
          -37.5968
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. BioBio \\/ Caupolican, San Carlos de Puren ",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "181"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2732,
          -37.5956
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Lautaro \\/ Millarrahue (Plaza), San Carlos de Puren",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "182"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2376,
          -37.5235
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Country Santa Catalina, Camino a Santa Barbara",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "Horario continuado",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "183"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3409,
          -37.4588
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Marconi \\/ Avda. Almirante Latorre (Supermercado UIMARC)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "281"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3379,
          -37.4692
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Alemania \\/ Avda. Marconi (Memorial Mártires de Antuco)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "282"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3325,
          -37.4505
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Nieves Vasquez \\/ Luxemburgo (Multicancha Villa Parque Sor Vicenta 1 - costado Punto Limpio",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "283"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3808,
          -37.4701
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Costanera Quilque Norte \\/ Nahuelbuta (Parques Nacionales - Punto Limpio)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "284"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.359,
          -37.4811
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Avda. Vicuña Mackenna \\/ Blanco Encalada (Salida EASY)",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "285"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.2736,
          -37.5979
        ]
      },
      "properties": {
        "Nombre": "Campana",
        "Ubicacion": "Ines de Suarez \\/ Pedro de Valdivia, San Carlos de Puren",
        "TipodePunto": "Campana",
        "Elementosquerecibe": "Botellas de vidrio",
        "Horario": "24 hrs",
        "Contacto": "Cristororeciclaje",
        "Emaildecontacto": "correo_cristororeciclaje@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "286"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3831,
          -37.4673
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Las Torcazas \\/ Aguas Calientes (CESFAM Nuevo Horizonte - Villa Los Profesores ). ",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "120"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3368,
          -37.4618
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Avda. Costanera Quilque Norte.",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "122"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.333,
          -37.4503
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Avda. Nieves Vasquez",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "123"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3658,
          -37.4773
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Condell, CESFAM 2 de Septiembre",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "124"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3767,
          -37.4797
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Misioneros, Altos del Retiro, sector Galilea",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "125"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3433,
          -37.4821
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "José Santos Ossa, Jardines de la Republica",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "126"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3283,
          -37.4907
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Rio Cali \\/ Avda. Ferrocarril (SECOF Villa Los Rios)",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "127"
      }
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [
          -72.3753,
          -37.4761
        ]
      },
      "properties": {
        "Nombre": "Punto Limpio",
        "Ubicacion": "Baquedano \\/ Agustinos, Sede JJVV Villa Galilea",
        "TipodePunto": "PuntoLimpio",
        "Elementosquerecibe": "Papel de oficina, diarios y revistas, carton, latas de aluminio de bebestibles, botellas plasticas PET 1 y de PEAD",
        "Horario": "Horario continuado",
        "Contacto": "Demarco",
        "Emaildecontacto": "correo_demarco@gmail.com",
        "Estado": "Habilitado",
        "Id_Punto": "128"
      }
    }
  ]
}