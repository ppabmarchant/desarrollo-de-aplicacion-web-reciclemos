# Desarrollo de aplicación web Reciclemos
_Se presentan los componentes y documentación de la aplicación web Reciclemos. Creada por Pablo Erick Marchant Oliva bajo la orientación del profesor Magister en Ingeniería Informática Aharon Cuevas Cordero en cumplimiento del requisito parcial para la obtención del título de Ingeniero Geomático_
Reciclemos fué creada para apoyar la gestión y publicación de la localización de puntos verdes en la comuna de Los Ángeles. Este sistema está orientado a los usuarios que requieren administrar una base de datos de puntos verdes, como también para aquellos que requieran obtener información visual de la localización de estos puntos. El sistema se ha desarrollado con tecnologías PHP/HTML/CSS. Los registros de puntos verdes son codificados en formato GeoJSON, para la representación se utiliza la API Leaflet de JavaScript y la cartografía es obtenida desde el servicio OpenStreetMap._

**Construido con:**
- PHP 7.323 y la API PDO_MYSLQ 5.02
- HTML
- CSS
- Bootstrap 4.5.3
- JavaScript
- API Leaflet de JavaScript 1.7.1

**Documentación:**
Se presenta el informe del proyecto de título en la carpeta "Documentacion", en el se presenta el contexto que justifica el desarrollo, el marco teórico y la metodología utilizada. 

**Pruebas del sistema:**

El sistema fué probado en un hosting con servidor web apache versión 2.4.46 y sistema gestor de base de datos MariaDB versión 10.145. Las pruebas de aceptación fueron realizadas en 
- Dispositivo de escritorio: Acer Nitro AN515-55
- Procesador: Intel Core i5-10300H CPU @2.50 GHz 2.50 GHz
- Memoria RAM instalada: 12GB

El acceso a la aplicación web se realizó por medio del navegador web Google Chrome versión 91.0.4472.164 mediante la URL https://www.nac64.cl/

**Instalación:**
- [Cargar el contenido de la carperta "WebApp Reciclemos" en el servidor donde será alojada] 
- [Importar la base de datos _nac82_puntos_reciclaje.sql_ contenida en la carpeta "Base de datos"] 
- [Modificar las credenciales de acceso a la base de datos de destino desde el archivo _conexion.php_ contenido en la carpeta "WebApp Reciclemos/modelo"] 
- [Abrir archivo index.php contenido en la carpeta "WebApp Reciclemos" para iniciar] 





